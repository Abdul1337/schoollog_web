<?php include 'header.php';?>
<!-- Banner Section -->
<div class="inner-page-header">
	<div class="full">
		<div class="inner-show-img"><img src="assets/images/contact_01.jpg"></div>
		<div class="container">
			<div class="slider-content">
				<h1>Contact Us</h1>
				<p class="subheading">India’s First AI Powered ERP For Schools</p>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="clearfix"></div>
</div>
<!-- //.Banner Section --> 
<!--Section-->
<section class="counication-page">
	<div class="container">
		<div class="contact-page" style="display:flex">
			<div class="contact-info">
				<div class="address">
					<h3>Phone Numbers</h3>
					<span class="contact-number">
						<span class="title">Support:</span>
						<span class="contact-no">8448443326</span>
					</span>
					<span class="contact-number">
						<span class="title">Sales:</span>
						<span class="contact-no">+91-7726005349</span>
					</span>
					<span class="contact-number">
						<span class="title">HR:</span>
						<span class="contact-no">+91- 9352591672</span>
					</span>
				</div>

				<div class="address">
					<h3>Corporate-Office</h3>
					<span class="contact-number">
						<span class="contact-no">C/409, Kamala Vihar ABCD
							<br />
							CHSL Mahavir Nagar
							<br />
							Kandivali, -West Mumbai
							<br />400067</span>
					</span>

				</div> 
				<div class="address">
					<h3>Branch-Office</h3>
					<span class="contact-number">
						<span class="contact-no">C-139,140 Dewan Plaza 2nd
							<br />Floor Narayan Vihar, Jaipur
							<br />-Rajasthan 302020</span>
					</span>
				</div>
			</div>
			<div class="contact-form">
				<!-- <h3>Apply Now For Free Demo</h3> -->
				<div class="calandly-iframe">
					<iframe width="100%" height="800px" src="https://calendly.com/slerp/15min?month=2020-01" frameborder="0"></iframe>
				</div>
			</div>
		</div>
	</div>

</section>
<!--//.Section-->


<!-- free-trial-section -->
<section class="free-trial-section wow fadeInUp">
	<?php 
		include 'freeTrialSection.php';
		freeTrialSection("Want to See","How to save time, reduce your workload<br/>and enhance learning?");
		?>
</section>
<!--// free-trial-section -->
<?php include 'footer.php';?>
