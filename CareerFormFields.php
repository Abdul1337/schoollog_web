<div class="form-group">
    <label>Name</label>
    <input type="text" class="form-control" name="name" placeholder="Enter Your Name" required />
</div>
<div class="form-group">
    <label>Mobile No.</label>
    <input type="text" pattern="^[0][1-9]\d{9}$|^[1-9]\d{9}$" class="form-control" name="mobile"
        placeholder="Enter Your Mobile Number" required />
</div>
<div class="form-group">
    <label>Email Id</label>
    <input type="email" class="form-control" name="email" placeholder="Enter Your Email ID" required />
</div>