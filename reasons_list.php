<?php include 'header.php';?>  
<!-- Banner Section --> 
    <div class="inner-page-header">
        <div class="full">
            <div class="inner-show-img"><img src="assets/images/108reasonswhy.png"></div>
            <div class="container">
                <div class="slider-content">
                    <h1>100 Reasons Why!</h1>
                    <p class="subheading">We give you 100 reasons why Schoollog is India's most
                        advanced automation solution catering all the needs of an educational institute.</p>
                </div>
            </div>
			<div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </div> 
<!-- Banner Section --> 

    <!--Section--> 
    <section class="counication-page our-team our-reason">

        <div class="container">
            <div class="reason-list">
                <div class="top-text">
                    <p class="big">Every day a new school management software is launching the
                        market. Are these all same? Are these all the right fit for your school? Let us
                        help you find the right one for your school.</p>
                </div>

                <div class="reason-108">
                    <ul>
                        <li><span class="counting">1. AI-powered</span><span class="counting-details"> Schoollog is
                                India's first AI-Powered ERP and LMS solution for schools.</span></li>
                        <li><span class="counting">2. All in one solution</span><span class="counting-details">
                                Schoollog is designed in a way to cover all the problems related to school management
                                and provide the best solution.</span></li>
                        <li><span class="counting">3. Affordable</span><span class="counting-details"> Schoollog offers
                                a big range of various affordable pricing plans suitable for all kinds of
                                schools.</span></li>
                        <li><span class="counting">4. User-friendly</span><span class="counting-details"> Schoollog Apps
                                and modules are super easy to use and work with.</span></li>
                        <li><span class="counting">5. Mobile Applications</span><span class="counting-details">
                                Schoollog offers apps for parents as well as all the major stakeholders at school to
                                make information available at fingertips.</span></li>
                        <li><span class="counting">6. Web-based portals</span><span class="counting-details"> There are
                                50+ role-based web portals available for all the stakeholders at school.</span></li>
                        <li><span class="counting">7. 24*7 support</span><span class="counting-details"> Our team is all
                                day and night ready to help you. </span></li>
                        <li><span class="counting">8. No hardware dependency</span><span class="counting-details"> Since
                                Schoollog is completely online no particular device is required to access data. One can
                                log in via a web browser on any device and access all the data.</span></li>
                        <li><span class="counting">9. Paperless</span><span class="counting-details"> As Schoollog is
                                cloud-based School doesn't need to maintain any registers or paper logs to maintain any
                                kind of data.</span></li>
                        <li><span class="counting">10. Time-savings</span><span class="counting-details"> Schoollog
                                helps schools in automating their day to day repetitive tasks and saves a major amount
                                of time making schools more productive.</span></li>
                        <li><span class="counting">11. Secure</span><span class="counting-details"> All the data is
                                Secured using Advanced Encryption and Portals are HTTPS Certified</span></li>
                        <li><span class="counting">12. Data-backup</span><span class="counting-details"> Data recovery
                                is Seamless and Hassel free with our data backups</span></li>
                        <li><span class="counting">13. SMS Integration</span><span class="counting-details"> Send alerts
                                via SMS to parents and teachers.</span></li>
                        <li><span class="counting">14. SaaS-enabled </span><span class="counting-details"> Schoollog is
                                a Software as a Service</span></li>
                        <li><span class="counting">15. AI-powered analysis</span><span class="counting-details">
                                Schoollog portals provide the student's test analysis report to enhance his/her learning
                                curve.</span></li>
                        <li><span class="counting">16. Performance tracking</span><span class="counting-details">
                                Schools can extract Student's as well as teacher's performance reports from time to time
                                and revise their teaching patterns. </span></li>
                        <li><span class="counting">17. Hardware compatible</span><span class="counting-details">
                                Schoollog portals can be logged in on any device having an internet connection.</span>
                        </li>
                        <li><span class="counting">18. Automated attendance</span><span class="counting-details"> With
                                the help of Biometric integration Student and teacher attendance is automated. Real-time
                                attendance updates are sent to parents and teachers automatically.</span></li>
                        <li><span class="counting">19. Digital diary</span><span class="counting-details"> Homework
                                updates sent to parents on the app.</span></li>
                        <li><span class="counting">20. School Gallery</span><span class="counting-details"> School can
                                add pictures and videos of events that will be shown in all the apps.</span></li>
                        <li><span class="counting">21. Instant alerts</span><span class="counting-details"> All the
                                important alerts can be sent to parents and teachers via in-app notifications.</span>
                        </li>
                        <li><span class="counting">22. Data management</span><span class="counting-details"> Data can be
                                uploaded to the schoollog software with any difficulty, similarily it is very easy to
                                export data from any module. </span></li>
                        <li><span class="counting">23. Custom reports</span><span class="counting-details"> More than
                                1000 customizable insightful reports can be generated on a click.</span></li>
                        <li><span class="counting">24. Leave management</span><span class="counting-details"> Manage
                                leaves of teachers and students, automate leave requests, track staff's leaves and
                                automatically generate salaries.</span></li>
                        <li><span class="counting">25. Fee management</span><span class="counting-details"> No need to
                                use extra accounts software to manage your fees, manage all your fee transactions with
                                zero error possibility with Schoollog Accounts.</span></li>
                        <li><span class="counting">26. Custom URL website</span><span class="counting-details"> School's
                                website hosted by Schoollog on the custom URL chose by the school.</span></li>
                        <li><span class="counting">27. Certificate generation</span><span class="counting-details">
                                Schoollog allows the school to generate certificates like Transfer certificate,
                                migration certificate, etc</span></li>
                        <li><span class="counting">28. Hassle-free report card generation</span><span
                                class="counting-details"> Schoollog allows the school to generate report cards on a
                                click with templates of all the boards.</span></li>
                        <li><span class="counting">29. Transport management</span><span class="counting-details"> A
                                simple tool to help you manage your school transports effortlessly.</span></li>
                        <li><span class="counting">30. Hostel management</span><span class="counting-details"> It allows
                                you to allocate different rooms and beds to different students and manage all the
                                necessary details of the schools' hostel. </span></li>
                        <li><span class="counting">31. Biometric Integration</span><span class="counting-details"> Gives
                                your school an extra edge with smart attendance management.</span></li>
                        <li><span class="counting">32. Expense management</span><span class="counting-details"> Easy and
                                manageable tool to manage the daily expenses of the school.</span></li>
                        <li><span class="counting">33. Inventory management</span><span class="counting-details"> An
                                easy to use stock management system for your school assisting you with supervising and
                                assuring of the ordering, storage, and use of components.</span></li>
                        <li><span class="counting">34. HR and Payroll management</span><span class="counting-details">
                                Manage staff effectively and with the integration of payment gateways and bank account
                                details of the entire staff, salaries can be credited promptly.</span></li>
                        <li><span class="counting">35. OTP authentication login</span><span class="counting-details">
                                Login with two-step authentication with OTP.</span></li>
                        <li><span class="counting">36. Enquiry management</span><span class="counting-details">
                                Schoollog help school manage enquiries via reception portal and reduces admission
                                hussle. </span></li>
                        <li><span class="counting">37. Library management</span><span class="counting-details"> A simple
                                and powerful solution to automate your school libraries. </span></li>
                        <li><span class="counting">38. Website integration</span><span class="counting-details"> Get a
                                website management console and manage your website on your own efficiently.</span></li>
                        <li><span class="counting">39. Technical assistance</span><span class="counting-details"> A
                                dedicated support team working tirelessly to resolve any queries you may have regarding
                                any module in our product.</span></li>
                        <li><span class="counting">40. On-site training</span><span class="counting-details"> No extra
                                cost for on-site training provided to get you fully ready to use Schoollog. </span></li>
                        <li><span class="counting">41. Exam management</span><span class="counting-details"> Structure
                                your exam and just 10 mins and get your report cards printed in just a click. </span>
                        </li>
                        <li><span class="counting">42. Admit card generator</span><span class="counting-details"> Save
                                your valuable time by generating admit cards for every exam with Schoollog exam
                                manager.</span></li>
                        <li><span class="counting">43. Day-wise absent report</span><span class="counting-details"> Get
                                an easily manageable customized staff and students absentees reports. No more hustle in
                                tracking absent students. </span></li>
                        <li><span class="counting">44. SMS usage report</span><span class="counting-details"> Get a
                                consolidated report of all the SMS used sent by the school with all the details. </span>
                        </li>
                        <li><span class="counting">45. Director approval for fine and concession</span><span
                                class="counting-details"> No worry of any fine or concession applied on students without
                                acknowledgement of the higher authorities of the school.</span></li>
                        <li><span class="counting">46. Digital leave application</span><span class="counting-details">
                                Students and staff can apply for leave with the app only and the school administration
                                can approve, decline and keep track of all the applications. </span></li>
                        <li><span class="counting">47. School annual calendar</span><span class="counting-details">
                                School can update the annual calendar within the parent app and the parents will stay
                                updated for all the events and holidays in advance. </span></li>
                        <li><span class="counting">48. Data update via excel</span><span class="counting-details">
                                Allows you to update your student and staff data with your existing excels in just a few
                                minutes. </span></li>
                        <li><span class="counting">49. Real-time attendance alert to parents</span><span
                                class="counting-details"> We maintain parents' peace of mind by keeping them updated
                                with their child's real-time attendance status. </span></li>
                        <li><span class="counting">50. Teacher performance report</span><span class="counting-details">
                                We help you track your teachers' performance with a single click. </span></li>
                        <li><span class="counting">51. Automatic birthday notifications</span><span
                                class="counting-details"> Hard to remember all your staff's and student's birthdays? No
                                more worries now! On your behalf, we make sure they get wished on their birthdays.
                            </span></li>
                        <li><span class="counting">52. Academic year login facility</span><span
                                class="counting-details"> Access your data from whichever academic session you want to,
                                whenever you want to. </span></li>
                        <li><span class="counting">53. Enquiry follow-up reminders</span><span class="counting-details">
                                Never miss any admission enquiry with schoollog enquiry manager, convert all your
                                potential leads. </span></li>
                        <li><span class="counting">54. Automatic messages to enquires</span><span
                                class="counting-details"> Don't want to lose touch with your admission enquires? We have
                                got you covered! All the admission enquires will be notified timely with automatic
                                messages. </span></li>
                        <li><span class="counting">55. Balance sheet</span><span class="counting-details"> Get a close
                                watch on every financial transaction by generating your balance sheet in a go!</span>
                        </li>
                        <li><span class="counting">56. Manage vouchers and challans</span><span
                                class="counting-details">Schoollog ERP enables you to Manage vouchers and challans for
                                academic rewards or concessions</span></li>
                        <li><span class="counting">57. Manage multiple companies</span><span class="counting-details">
                                Schoollog allows schools to manage multiple companies with single accounts portal.
                            </span></li>
                        <li><span class="counting">58. Cheque handling</span><span class="counting-details">Schoollog
                                payroll system allows you to manage and handle payments via cheques</span></li>
                        <li><span class="counting">59. Automatic fine handling</span><span class="counting-details">Fine
                                management system for handling fines </span></li>
                        <li><span class="counting">60. Collection reports</span><span class="counting-details"> Provides
                                daily fee collection reports to the director on his phone.</span></li>
                        <li><span class="counting">61. Auto ledger</span><span class="counting-details">Automatically
                                generated ledger reports for Accounts / Collections / Incomes etc</span></li>
                        <li><span class="counting">62. Multilingual SMS</span><span class="counting-details"> Send
                                notifications to parents via SMS in English as well as Hindi.</span></li>
                        <li><span class="counting">63. Fee due reminders</span><span class="counting-details"> Parents
                                are kept updated via in-app fee due notification to get schools fee collection done on
                                time. </span></li>
                        <li><span class="counting">64. Birthday list</span><span class="counting-details"> Get the list
                                of birthdays of staff and students in a single place. </span></li>
                        <li><span class="counting">65. Refund report</span><span class="counting-details"> Print
                                daily,monthly log of refunds given to students.</span></li>
                        <li><span class="counting">66. Grade conversion</span><span class="counting-details"> Get the
                                marks converted into grades automatically, without any extra effort. </span></li>
                        <li><span class="counting">67. Gate pass</span><span class="counting-details"> Promise every
                                student's security by managing school visitors via schoollog gate pass.</span></li>
                        <li><span class="counting">68. GST fee handling</span><span class="counting-details"> School can
                                sell all Inventory related items with GST handling </span></li>
                        <li><span class="counting">69. Share photos and videos</span><span class="counting-details">
                                School director, Admin, Class teachers can share the school event's pictures and videos
                                with parents via the app. </span></li>
                        <li><span class="counting">70. Payment gateway</span><span class="counting-details"> Parents can
                                pay Stutent's fee via multiple online payment gateways. </span></li>
                        <li><span class="counting">71. Data export</span><span class="counting-details"> Exporting any
                                kind of data whenever you need, is now a matter of few minutes. </span></li>
                        <li><span class="counting">72. Easy data import</span><span class="counting-details"> Data from
                                any other software can be easily imported to schoollog without any hustle. </span></li>
                        <li><span class="counting">73. All board exam templates</span><span class="counting-details">
                                CBSE, State board Schools can easily manage exam with Schoollog. </span></li>
                        <li><span class="counting">74. All board report card templates</span><span
                                class="counting-details"> Schoollog comes with all State Boards, CBSE ready report card
                                templates.</span></li>
                        <li><span class="counting">75. Test-wise analysis</span><span class="counting-details">
                                Parent/Teacher can view testowise Analysis and Report. </span></li>
                        <li><span class="counting">76. Defaulter follow-up mechanism</span><span
                                class="counting-details"> Defaulter follow-up mechanism helps Schools pace-up fee
                                collection. </span></li>
                        <li><span class="counting">77. No hardware dependency</span><span class="counting-details"> one
                                can log in and access any schoollog portal to any system connected to the internet from
                                anywhere. </span></li>
                        <li><span class="counting">78. Custom domain for your website</span><span
                                class="counting-details"> Get a website with a domain of your wish. </span></li>
                        <li><span class="counting">79. Custom App for Android/iOS</span><span class="counting-details">
                                Schools can get a Custom App for their Schools for better branding and Customized
                                experience of the product </span></li>
                        <li><span class="counting">80. New session promotion</span><span class="counting-details">
                                Schools can manage multi session record easily.</span></li>
                        <li><span class="counting">81. Role-based logins</span><span class="counting-details"> Allows
                                different users to log in separately to maintain data privacy among different users.
                            </span></li>
                        <li><span class="counting">82. Timely updates</span><span class="counting-details"> Every month
                                new features are added to all the Schoollog portals.</span></li>
                        <li><span class="counting">83. Paytm integration</span><span class="counting-details"> No need
                                to rush to school to pay fees, parents can pay students fee via Paytm. Helps schools in
                                pacing up fee collection.</span></li>
                        <li><span class="counting">84. 1000+ exportable reports</span><span class="counting-details">
                                Export every kind of report related to administration, accounts or admissions enquiry in
                                a go.</span></li>
                        <li><span class="counting">85. Print-ready reports</span><span class="counting-details"> Reports
                                are hand-crafted keeping in view all the necessory government Compliances. </span></li>
                        <li><span class="counting">86. Hardware Compatibility</span><span class="counting-details">
                                Schoollog is compatible with both laser printers and dot matrix printers enabling school
                                to use your existing print technology effortlessly. </span></li>
                        <li><span class="counting">87. Mode Of Payment</span><span class="counting-details"> Schoollog
                                offers different modes of payment to parents like payment through cash, cheque, challan,
                                debit card, credit card and net banking.</span></li>
                        <li><span class="counting">88. Technology aided Schooling</span><span class="counting-details">
                                Schools can easily empower there Communication, Academics, Management through technology
                                via Schoollog. </span></li>
                        <li><span class="counting">89. Multi-School Solution</span><span class="counting-details">
                                Schoollog is designed in a way that you can handle all the branches and groups of an
                                institution with a single super-admin portal.</span></li>
                        <li><span class="counting">90. Fee Receipts</span><span class="counting-details"> Allows to
                                create fees receipts to print or share digitally with parents.</span></li>
                        <li><span class="counting">91. Secure Password Management</span><span class="counting-details">
                                Easily recover lost passwords or simply change your passwords from time to time for
                                complete security.</span></li>
                        <li><span class="counting">92. Multiple Boards of Education</span><span
                                class="counting-details"> Schoollog best suits for schools affiliated to multiple boards
                                of education, such as CBSE, ICSE and all the other state boards.</span></li>
                        <li><span class="counting">93. Improves Productivity</span><span class="counting-details"> with
                                the help of Schoollog schools can cut-down there time spent in Management by 30%.
                            </span></li>
                        <li><span class="counting">94. Reliable</span><span class="counting-details"> Schoollog is loved
                                by more than 1 lac students, parents, and schools, making it the most trusted ERP for
                                schools.</span></li>
                        <li><span class="counting">95. Online Help</span><span class="counting-details"> One can learn
                                using Schoollog at any time, from anywhere with the help of Schoollog tutorial
                                videos.</span></li>
                        <li><span class="counting">96. Apps for all</span><span class="counting-details"> Schoollog apps
                                are available on Google play store as well as Appstore.</span></li>
                        <li><span class="counting">97. 1 lac+ teachers and parents</span><span class="counting-details">
                                Schoollog is used by more than 1 lac teachers and parents across India.</span></li>
                        <li><span class="counting">98. Presence in 4 states</span><span class="counting-details">
                                Schoollog has been successfully catering to the best schools in states like Rajasthan,
                                Haryana, Uttar pradesh and Assam.</span></li>
                        <li><span class="counting">99. An initiative by IITians</span><span class="counting-details">
                                Schoollog is an initiative by the alumni of IIT Ropar striving to improve the learning
                                curve of every child. </span></li>
                        <li><span class="counting">100. Established since 2015</span><span class="counting-details">
                                Schoollog has been successfully catering to more than 1000 schools.</span></li>
                    </ul>
                </div>
            </div>
        </div>

    </section>
    <!--//.Section end--> 

<!-- free-trial-section -->
<section class="free-trial-section wow fadeInUp">
	 <?php 
		include 'freeTrialSection.php';
		freeTrialSection("100 reasons Why!","We have given you 100 reasons to love us, give us 1 chance to help you take your school management to another level.");
		?>
</section> 
<!--// free-trial-section --> 
<?php include 'footer.php';?>






 