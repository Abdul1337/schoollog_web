<?php include 'header.php';?>  

 <!-- Banner Section -->

    <div class="inner-page-header"> 
        <div class="full">
            <div class="inner-show-img"><img src="assets/images/termsAndConditation.jpg"></div>
            <div class="container">
                <div class="slider-content">
                    <h1>Terms and Conditions</h1>
                    <p class="subheading">India’s First AI Powered ERP For Schools</p>
                </div>
            </div>
			<div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </div>


    <!-- //.Banner Section -->

    <!--Section-->

    <section class="counication-page our-team our-reason">

        <div class="container">
            <div class="reason-list">
                <div class="top-text">
                    <p>By signing this Agreement, the School is agreeing to be bound by the
                        following terms and conditions ("Terms of Service"). The Service provider
                        reserves the right to update and change the Terms of Service from time to time
                        without notice. Any new features that augment or enhance the current Service,
                        including the release of new tools and resources, shall be subject to the Terms
                        of Service. Continued use of the Service after any such changes shall constitute
                        your consent to such changes. Violation of any of the terms below will result in
                        the termination of your Account. While service provider prohibits such conduct
                        and Content on the Service, you understand and agree that the service provider
                        cannot be responsible for the Content posted on the Service and you nonetheless
                        may be exposed to such materials. You agree to use the Service at your own risk.</p>
                </div>

                <div class="reason-108 terms">
                    <h4>Schoollog Terms of Service</h4>
                    <ul class="terms_list">
                        <li>Please read this agreement ("TOS") carefully. By using or accessing any
                            Service, you (as the "User") agree to the terms of this TOS. "Service" means the
                            Schoollog service. If you do not agree, do not use this Service.</li>

                        <li>This TOS is a legal agreement between you and Schoollog This TOS governs the
                            Service, and content available through it. It also governs support services (if
                            any) available as part of your subscription. If a separate or supplemental TOS
                            appears when you access any element of the Service, its terms will control as to
                            that element.</li>

                        <li>Schoollog reserves the right to update and change the Terms of Service from
                            time to time without notice. Any new features that augment or enhance the
                            current Service, including the release of new tools and resources, shall be
                            subject to the Terms of Service. Continued use of the Service after any such
                            changes shall constitute your consent to such changes. You can review the most
                            current version of the Terms of Service at any time at www.Schoollog/terms.</li>

                        <li>Violation of any of the terms below will result in the termination of your
                            Account. While Schoollog prohibits such conduct and Content on the Service, you
                            understand and agree that Schoollog cannot be responsible for the Content posted
                            on the Service and you nonetheless may be exposed to such materials. You agree
                            to use the Service at your own risk.</li>
                    </ul>

                    <h4>Account Terms</h4>
                    <ul class="terms_list">
                        <li>You must be a human. Accounts registered by “bots” or other automated
                            methods are not permitted.</li>

                        <li>You must provide your legal full name, a valid email address, and any other
                            information requested in order to complete the signup process.</li>

                        <li>Your login may only be used by one person – a single login shared by
                            multiple people is not permitted. You may create separate logins for as many
                            people as you would like.</li>

                        <li>You are responsible for maintaining the security of your account and
                            password. Schoollog cannot and will not be liable for any loss or damage from
                            your failure to comply with this security obligation.</li>

                        <li>You are responsible for all Content posted and activity that occurs under
                            your account (even when Content is posted by others who have accounts under your
                            account).</li>

                        <li>One person or legal entity may not maintain more than one free account.</li>

                        <li>You may not use the Service for any illegal or unauthorized purpose. You
                            must not, in the use of the Service, violate any laws in your jurisdiction
                            (including but not limited to copyright laws).</li>

                    </ul>

                    <h4>Payment, Refunds, Upgrading and Downgrading Terms</h4>
                    <ul class="terms_list">
                        <li>You must represent a school or a group of schools (“School”) in order to
                            qualify for the 30 days free trial period.</li>

                        <li>Your School can fully utilize the 1-month free trial period to examine all
                            features of the Service.</li>

                        <li>If you need more time beyond the 1-month free trial period, contact
                            Schoollog and tell us why. We reserve the right to refuse or approve the
                            extension request.</li>

                        <li>After the free trial period has ended, your School may choose to terminate
                            the account or continue on a paying account.</li>

                        <li>Once your account is converted to a paying account, your free trial period
                            is over. You will be billed for your first month immediately upon conversion</li>

                        <li>The Service is billed in advance on a monthly basis. There will be no
                            refunds or credits for partial months of service, upgrade/downgrade refunds, or
                            refunds for months unused with an open account. For other reasons not mentioned
                            above, then refunds will be given at the discretion of the Company Management.</li>

                        <li>Calculation of how much you are billed is based on the number of students
                            enrolled in the Schoollog student management feature on the bill date. Students
                            whose records have been marked in the system as having left the school or as
                            having been deleted do not count towards the student total.</li>

                        <li>Monthly bills are payable in full 10 days after the bill date where checks
                            are used to make payments.</li>

                        <li>You can cancel at any time. No further payments are required from you upon
                            account cancellation.</li>

                        <li>All fees are exclusive of all taxes, levies, or duties imposed by taxing
                            authorities, and you shall be responsible for payment of all such taxes, levies,
                            or duties.</li>

                        <li>For any upgrade or downgrade in plan level, you will automatically be billed
                            the new rate on your next billing cycle.</li>
                        <li>Downgrading your Service may cause the loss of Content, features, or
                            capacity of your Account. Schoollog does not accept any liability for such loss.</li>
                    </ul>

                    <h4>Cancellation and Termination</h4>
                    <ul class="terms_list">
                        <li>You are solely responsible for properly cancelling your account, which can
                            be done via email to cancel at Schoollog.</li>

                        <li>All of your Content will be immediately backed up upon cancellation and will
                            be transferred to you.</li>

                        <li>If you cancel the Service before the end of your current paid up month, your
                            cancellation will take effect immediately and you will not be charged again.</li>

                    </ul>

                    <h4>Modifications to the Service and Prices</h4>
                    <ul class="terms_list">
                        <li>YSchoollog reserves the right at any time and from time to time to modify,
                            temporarily or permanently, the Service (or any part thereof) with or without
                            notice.</li>

                        <li>Prices of all Services, including but not limited to monthly subscription
                            plan fees to the Service, are subject to change upon 30 days notice from us.
                            Such notice may be provided at any time by posting the changes to the Schoollog
                            site.</li>

                        <li>Schoollog shall not be liable to you or to any third party for any
                            modification, price change, suspension or discontinuance of the Service.</li>

                    </ul>

                    <h4>Copyright and Content Ownership</h4>
                    <ul class="terms_list">
                        <li>We claim no intellectual property rights over the material you provide to
                            the Service. Your profile and materials uploaded remain yours.</li>

                        <li>Schoollog does not pre-screen Content, but Schoollog and its designee have
                            the right (but not the obligation) in their sole discretion to refuse or remove
                            any Content that is available via the Service.</li>

                        <li>The look and feel of the Service is copyright©2019-2020 Schoollog All rights
                            reserved. You may not duplicate, copy, or reuse any portion of the HTML/CSS,
                            Flash pages or visual design elements without express written permission from
                            Schoollog</li>

                    </ul>

                    <h4>General Conditions</h4>
                    <ul class="terms_list">
                        <li>Technical support is only provided to paying account holders and is
                            available via email and telephonic conversation.</li>

                        <li>You understand that Schoollog uses third party vendors and hosting partners
                            to provide the necessary hardware, software, networking, storage, and related
                            technology required to run the Service.</li>

                        <li>You must not modify, adapt or hack the Service or modify another website so
                            as to falsely imply that it is associated with the Service, Schoollog, or any
                            other Schoollog service.</li>

                        <li>You agree not to reproduce, duplicate, copy, sell, resell or exploit any
                            portion of the Service, use of the Service, or access to the Service without the
                            express written permission by Schoollog.</li>

                        <li>We may, but have no obligation to, remove Content and Accounts containing
                            Content that we determine in our sole discretion are unlawful, offensive,
                            threatening, libelous, defamatory, pornographic, obscene or otherwise
                            objectionable or violates any party’s intellectual property or these Terms of
                            Service.</li>

                        <li>Verbal, physical, written or other abuse (including threats of abuse or
                            retribution) of any Schoollog customer, employee, member, or officer will result
                            in immediate account termination.</li>

                        <li>You understand that the technical processing and transmission of the
                            Service, including your Content, may be transferred unencrypted and involve (a)
                            transmissions over various networks; and (b) changes to conform and adapt to
                            technical requirements of connecting networks or devices.</li>

                        <li>You must not upload, post, host, or transmit unsolicited email, SMSs, or
                            “spam” messages.</li>

                        <li>You must not transmit any worms or viruses or any code of a destructive nature.</li>

                        <li>If your bandwidth significantly exceeds the average bandwidth usage (as
                            determined solely by Schoollog) of other Schoollog customers, we reserve the
                            right to immediately disable your account or throttle your file hosting until
                            you can reduce your bandwidth consumption.</li>

                        <li>Schoollog does not warrant that (i) the service will meet your specific
                            requirements, (ii) the service will be uninterrupted, timely, secure, or
                            error-free, (iii) the results that may be obtained from the use of the service
                            will be accurate or reliable, (iv) the quality of any products, services,
                            information, or other material purchased or obtained by you through the service
                            will meet your expectations, and (v) any errors in the Service will be
                            corrected.</li>

                        <li>You expressly understand and agree that Schoollog shall not be liable for
                            any direct, indirect, incidental, special, consequential or exemplary damages,
                            including but not limited to, damages for loss of profits, goodwill, use, data
                            or other intangible losses (even if Schoollog has been advised of the
                            possibility of such damages), resulting from: (i) the use or the inability to
                            use the service; (ii) the cost of procurement of substitute goods and services
                            resulting from any goods, data, information or services purchased or obtained or
                            messages received or transactions entered into through or from the service;
                            (iii) unauthorized access to or alteration of your transmissions or data; (iv)
                            statements or conduct of any third party on the service; (v) or any other matter
                            relating to the service.</li>

                        <li>The failure of Schoollog to exercise or enforce any right or provision of
                            the Terms of Service shall not constitute a waiver of such right or provision.
                            The Terms of Service constitutes the entire agreement between you and Schoollog
                            and govern your use of the Service, superseding any prior agreements between you
                            and Schoollog (including, but not limited to, any prior versions of the Terms of
                            Service).</li>

                        <li>Questions about the Terms of Service should be sent to: info@Schoollog.in</li>

                    </ul>

                </div>
            </div>
        </div>
    </section>
    <!--//.Section-->


<!-- free-trial-section -->
<section class="free-trial-section wow fadeInUp">
	 <?php 
		include 'freeTrialSection.php';
		freeTrialSection("Want to See","How to save time, reduce your workload
and enhance learning?");
		?>
</section> 
<!--// free-trial-section --> 
<?php include 'footer.php';?>

