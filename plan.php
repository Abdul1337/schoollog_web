<?php include 'header.php';?>  
    <div class="inner-page-header"> 
        <div class="full">
            <div class="inner-show-img"><img src="assets/images/inner-img01.jpg"></div>
            <div class="container">
                <div class="slider-content">
                    <h1>Honest Plan, No Surprise</h1>
                    <p class="subheading">India’s First AI Powered ERP For Schools</p>
                </div>
            </div>
			<div class="clearfix"></div>
        </div>
        
		<div class="clearfix"></div>
    </div>
 
    <!--Plan-section-->
    <section class="plan-main-section">
        <div class="container">
            <div class="heading-main">
                <h2>Why Choose Us?</h2>
                <p class="subheading">India’s First AI Powered ERP For Schools</p>
            </div>
            <div class="plan-main">
                <div class="slide-carousel">
                    <div onmouseout="remove_highlight()" onmouseover="highlight('STANDARD')" id="STANDARD"
                        class="plan-first">
                        <h3>STANDARD</h3>
                        <div class="icon-main">
                            <span class="icon-inner">
                               <i class="icon-lunch"></i>
                            </span>

                        </div>

                        <ul class="benefit-list">
                            <li>
                                <i class="icon-price_01"></i>
                                <span>Schoollog Powered App</span></li>
                            <li>
                               <i class="icon-price_02"></i>
                                <span>CMS Ready Website</span></li>
                            <li>
                                <i class="icon-price_03"></i>
                                <span>Parent App, Teacher App</span></li>
                            <li>
                                <i class="icon-price_04"></i>
                                <span>10 Sms Free Pspm</span></li>
                            <li>
                                <i class="icon-price_05"></i>
                                <span>24*7 Online Support</span></li>
                        </ul>

                        <div class="price">
                            <big>₹10</big>
                            <small>Per month, per student</small>
                        </div>
                        <a href="https://schoollog.in/landingpage/" target="_blank" class="buy-now-btn">Buy Now</a>
                    </div>

                    <div onmouseout="remove_highlight()" onmouseover="highlight('PREMIUM')" id="PREMIUM"
                        class="plan-first highlight">
                        <h3>PREMIUM</h3>
                        <div class="icon-main">
                            <span class="icon-inner">
                               <i class="icon-grow_icon01"></i>
                            </span>

                        </div>

                        <ul class="benefit-list">
                            <li>
                                <i class="icon-price_01"></i>
                                <span>All of Standard+</span>
                            </li>
                            <li>
                                <i class="icon-price_02"></i>
                                <span>Schools Branded Apps</span></li>
                            <li>
                                <i class="icon-price_03"></i>
                                <span>Admin App, Principal App, Director App</span></li>
                            <li>
                                <i class="icon-price_04"></i>
                                <span>12 Sms Free Pspm</span></li>
                            <li>
                                <i class="icon-price_05"></i>
                                <span>Alexa Integration</span></li>
                        </ul>

                        <div class="price">
                            <big>₹12</big>
                            <small>Per month, per student</small>
                        </div>
                        <a href="https://schoollog.in/landingpage/" target="_blank" class="buy-now-btn">Buy Now</a>
                    </div>

                    <div onmouseout="remove_highlight()" onmouseover="highlight('ENTERPRISE')" id="ENTERPRISE"
                        class="plan-first">
                        <h3>ENTERPRISE</h3>
                        <div class="icon-main enterprise">
                            <span class="icon-inner">
                               <i class="icon-building"></i>
                            </span>

                        </div>

                        <ul class="benefit-list">
                            <li>
                                <i class="icon-price_01"></i>
                                <span>Al of Premium+</span></li>
                            <li>
                                <i class="icon-price_02"></i>
                                <span>Premium Features</span></li>
                            <li>
                                <i class="icon-price_03"></i>
                                <span>Voice Sms Integration</span></li>
                            <li>
                                <i class="icon-price_04"></i>
                                <span>15 Sms Free Pspm</span></li>
                            <li>
                                <i class="icon-price_05"></i>
                                <span>Biometric Integration</span></li>
                        </ul>

                        <div class="price">
                            <big>₹15</big>
                            <small>Per month, per student</small>
                        </div>
                        <a href="https://schoollog.in/landingpage/" target="_blank" class="buy-now-btn">Buy Now</a>
                    </div>
                </div>
            </div>

        </div>
        <div class="map-tab-bottom">
            <div class="container">
                <div class="awesome-app-list-tabs-col">
                    <ul class="nav nav-tabs awesome-app-list-tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#admin-app-list-tabs-1" role="tab">
                                <div class="awesome-app-list-nav-link">
                                    <span class="grow-tab-icon">
                                        <i class="icon-map_tab_02"></i>
                                    </span>

                                    <p>
                                        Good market Presence
                                    </p>
                                </div>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#admin-app-list-tabs-2" role="tab">
                                <div class="awesome-app-list-nav-link">
                                    <span class="grow-tab-icon">
                                        <i class="icon-user"></i>

                                    </span>
                                    <p>
                                        1000+ Schools
                                    </p>
                                </div>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#admin-app-list-tabs-3" role="tab">
                                <div class="awesome-app-list-nav-link">
                                    <span class="grow-tab-icon">
                                        <i class="icon-map_tab_04"></i>
                                    </span>

                                    <p>
                                        Affordable for all
                                    </p>
                                </div>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#admin-app-list-tabs-4" role="tab">
                                <div class="awesome-app-list-nav-link">
                                    <span class="grow-tab-icon">
                                        <i class="icon-map_tab_03"></i>
                                    </span>
                                    <p>
                                        Onsite trainings and 24*7 online support
                                    </p>
                                </div>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#admin-app-list-tabs-5" role="tab">
                                <div class="awesome-app-list-nav-link">
                                    <span class="grow-tab-icon">
                                        <i class="icon-map_tab_05"></i>
                                    </span>
                                    <p>
                                        Get your school's own app ready in just 1 day
                                    </p>
                                </div>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#admin-app-list-tabs-6" role="tab">
                                <div class="awesome-app-list-nav-link">
                                    <span class="grow-tab-icon">
                                    	<i class="icon-calendar_001"></i>
                                    </span>
                                    <p>
                                        15 DAYS FREE TRIAL
                                    </p>
                                </div>
                            </a>
                        </li>
                    </ul>
                    <!-- Tab panes -->
                </div>
                <div class="awesome-app-list-content-col">
                    <div class="tab-content awesome-app-list-content">
                        <div class="tab-pane active" id="admin-app-list-tabs-1" role="tabpanel">
                            <h4>Good market Presence</h4>
                            <img style="margin-left:3vw" src="assets/images/schoollog_india.png" />
                        </div>

                        <div class="tab-pane" id="admin-app-list-tabs-2" role="tabpanel">
                            <h4>1000+ clients</h4>
                            <img style="margin-left:3vw" src="assets/images/schoollog_india.png" />

                        </div>
                        <div class="tab-pane" id="admin-app-list-tabs-3" role="tabpanel">
                            <h4>Affordable for all</h4>
                            <img style="margin-left:3vw" src="assets/images/schoollog_india.png" />

                        </div>

                        <div class="tab-pane" id="admin-app-list-tabs-4" role="tabpanel">
                            <h4>Onsite trainings and 24*7 online support</h4>
                            <img style="margin-left:3vw" src="assets/images/schoollog_india.png" />

                        </div>

                        <div class="tab-pane" id="admin-app-list-tabs-5" role="tabpanel">
                            <h4>Get your school's app ready in just 1 day</h4>
                            <img style="margin-left:3vw" src="assets/images/schoollog_india.png" />
                        </div>

                        <div class="tab-pane" id="admin-app-list-tabs-6" role="tabpanel">
                            <h4>15 DAYS FREE TRIAL</h4>
                            <img style="margin-left:3vw" src="assets/images/schoollog_india.png" />
                        </div>

                    </div>
                </div>
            </div>

            <!-- <div class="panel-group" id="accordion" role="tablist"
            aria-multiselectable="true"></div> -->
        </div>

    </section>
    <!--Plan-section end-->
   
<!-- free-trial-section -->
<section class="free-trial-section wow fadeInUp">
	 <?php 
		include 'freeTrialSection.php';
		freeTrialSection("Choose Us","We cost you even lesser than a biscuit packet! Amazed? Hit the button to know how.");
		?>
</section> 
<!--// free-trial-section --> 
<?php include 'footer.php';?>
<script type="text/javascript" src="assets/js/owl.carousel.min.js"></script>


