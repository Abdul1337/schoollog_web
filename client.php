
<?php include 'header.php';?>  

  <!-- Banner Section -->
    <div class="inner-page-header">
        
        <div class="full">
            <div class="inner-show-img"><img src="assets/images/clients.jpg"></div>
            <div class="container">
                <div class="slider-content">
                    <h1>Our Clients</h1>
                    <p class="subheading">India’s First AI Powered ERP For Schools</p>
                </div>
            </div>
			<div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </div>
    <!-- //.Banner Section -->
    <!--Plan-section-->
    <section class="counication-page client-page">
        <div class="container">
            <div class="testimonial">
                <div class="flexslider-client carousel">
                    <ul class="slides">
                        <div class="testimonail-carousel">
                            <li class="client-speak">
                                <div class="speaker_text_wrapper">
                                    <span class="speak-text">Schoollog allowed us to cut the school administration process
                                        down by giving us more control to manage our day to day recurring tasks. That is a
                                        powerful addition to our process. Everything is easy and simple to use. The team is
                                        very cooperative. They lead you through the whole process until you get hands-on
                                        with Schoollog.</span>
                                </div>
                                <span class="speaker-info"><span class="speaker-pic"><img
                                            src="assets/images/testimonials/client_01.jpg"></span><span
                                        class="speaker-information"><span class="name-text">Nakul vijay</span><span
                                            class="school-name">BSN ACADEMY</span></span></span>
                            </li>
                            <li class="client-speak">
                                <div class="speaker_text_wrapper">
                                    <span class="speak-text">Schoollog has given us an innovative, 100% online program that
                                        meets all our needs. All our school data is now secure We have been using Schoollog
                                        from so long, I have to say it has totally changed the way we used to run our
                                        school.</span>
                                </div>
                                <span class="speaker-info"><span class="speaker-pic"><img
                                            src="assets/images/testimonials/client_02.jpg"></span><span
                                        class="speaker-information"><span class="name-text">Anuj Yadav</span><span
                                            class="school-name">Ch.Sughar Singh Group of institution</span></span></span>
                            </li>
                            <li class="client-speak">
                                <div class="speaker_text_wrapper">
                                    <span class="speak-text">Schoollog has been a real saviour for our school administration
                                        as it has reduced management workload by 80% and saves a huge amount of time.I'm
                                        really happy with all the improvements and new features in Schoollog and surely
                                        recommends to every school.</span>
                                </div>
                                <span class="speaker-info"><span class="speaker-pic"><img
                                            src="assets/images/testimonials/client_03.jpg"></span><span
                                        class="speaker-information"><span class="name-text">Dr.Puneet Goyal</span><span
                                            class="school-name">OM INTERNATIONAL PUBLIC SCHOOL</span></span></span>
                            </li>
                            <li class="client-speak">
                                <div class="speaker_text_wrapper">
                                    <span class="speak-text">Schoollog is just beyond our expectations in terms of service.
                                        They deliver what they say. The installation and implementation process takes no
                                        time and is completely hassle-free. With the previous software that we were using
                                        most of our time was eaten up in just learning to use the software.</span>
                                </div>
                                <span class="speaker-info"><span class="speaker-pic"><img
                                            src="assets/images/testimonials/client_04.jpg"></span><span
                                        class="speaker-information"><span class="name-text">Mrs.Aruna Suresh</span><span
                                            class="school-name">S R GLOBAL SCHOOL</span></span></span>
                            </li>
                            <li class="client-speak">
                                <div class="speaker_text_wrapper">
                                    <span class="speak-text">Schoollog is just beyond our expectations in terms of service.
                                        They deliver what they say. The installation and implementation process takes no
                                        time and is completely hassle-free. With the previous software that we were using
                                        most of our time was eaten up in just learning to use the software.</span>
                                </div>
                                <span class="speaker-info"><span class="speaker-pic"><img
                                            src="assets/images/testimonials/client_05.jpg"></span><span
                                        class="speaker-information"><span class="name-text">Mr. Anil Garg</span><span
                                            class="school-name">VAISH SR. SEC. SCHOOL BAUND KALAN,
                                            bhiwani</span></span></span>
                            </li>
                            <li class="client-speak">
                                <div class="speaker_text_wrapper">
                                    <span class="speak-text">Schoollog is one of the most useful ERP software in which
                                        institutes can fulfil their need as they require. We have been using Schoollog since
                                        last one year. Schoollog has so many features that make it different from other
                                        software.Schoollog is one of the most useful ERP software in which institutes can
                                        fulfil their need as they require. We have been using Schoollog since last one year.
                                        Schoollog has so many features that make it different from other software.</span>
                                </div>
                                <span class="speaker-info"><span class="speaker-pic"><img
                                            src="assets/images/testimonials/client_06.jpg"></span><span
                                        class="speaker-information"><span class="name-text">Mr. Shubhkaran</span><span
                                            class="school-name">MATRIX HIGH SCHOOL, sikar</span></span></span>
                            </li>
                            <li class="client-speak">
                                <div class="speaker_text_wrapper">
                                    <span class="speak-text"> We have tried out a lot of other ERPs for our school but none
                                        has eased out our process and reduced our expenses as Schoollog has done. Seeing the
                                        results makes me agree on the fact that all that I'm paying for schoollog is totally
                                        worth it. We rarely get issues in using Schoollog, but if there is any their support
                                        team get it resolved in no time.</span>
                                </div>
                                <span class="speaker-info"><span class="speaker-pic"><img
                                            src="assets/images/testimonials/client_07.jpg"></span><span
                                        class="speaker-information"><span class="name-text">Stuti Gupta</span><span
                                            class="school-name">SS B R L VAISH RES. EDU.PUB. ACADEMY,
                                            mainpuri</span></span></span>
                            </li>
                            <li class="client-speak">
                                <div class="speaker_text_wrapper">
                                    <span class="speak-text">We have founded our productive aspect of Schoollog. Letting the
                                        parents know about their child's daily attendance and homework is improving our
                                        brand value through the while labelled app of our school from Schoollog and the fee
                                        defaulter management and follow up system helped us managing our business's internal
                                        economic factors of business environment. Thank you Schoollog </span>
                                </div>
                                <span class="speaker-info"><span class="speaker-pic"><img
                                            src="assets/images/testimonials/client_08.jpg"></span><span
                                        class="speaker-information"><span class="name-text">Mr. Anil Bhawaria</span><span
                                            class="school-name">SURAJ SMART SCHOOL</span></span></span>
                            </li>
                            <li class="client-speak">
                                <div class="speaker_text_wrapper">
                                    <span class="speak-text">From student enrollment to automated attendance management and
                                        fee collection, all the Schoollog portals are befitting for our school management
                                        requirements. Also, the Apps have been designed for easy use by a teacher or a
                                        parent. I would recommend Schoollog to any school that believes in
                                        excellence.</span>
                                </div>
                                <span class="speaker-info"><span class="speaker-pic"><img
                                            src="assets/images/testimonials/client_09.jpg"></span><span
                                        class="speaker-information"><span class="name-text">Mr. Sanjay Kulhar</span><span
                                            class="school-name">SKISHORI INTERNATIONAL SCHOOL,
                                            jhunjhunu</span></span></span>
                            </li>
                            <li class="client-speak">
                                <div class="speaker_text_wrapper">
                                    <span class="speak-text">Managing a big institution such as ours is in itself a
                                        challenging task, with Schoollog we have managed to pace up our fee collection by
                                        30%. Now, i am able to keep a track on the daily activities from my mobile
                                        itself.</span>
                                </div>
                                <span class="speaker-info"><span class="speaker-pic"><img
                                            src="assets/images/testimonials/client_10.jpg"></span><span
                                        class="speaker-information"><span class="name-text">Mr. Ramkaran Meel</span><span
                                            class="school-name">GURUKRIPA CAREER INSTITUTE</span></span></span>
                            </li>
                        </div>
                    </ul>
                </div>
            </div>
        </div>
        <div class="map-tab-bottom why-reasons career-section client-logo-slide">
            <div class="container">
                <div class="flexslider carousel">
                    <div class="our-client-logos"><h2>Our Clients<h2></div>
                    <!-- Open below ul to blow your mind -->
                    <ul class="slides">
                        <div class="client-logo-carousel">
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_1.jpg"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_2.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_3.jpg"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_4.jpg"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_5.jpg"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_6.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_7.jpg"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_8.jpg"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_9.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_10.jpg"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_11.jpg"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_12.jpg"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_13.jpg"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_14.jpg"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_15.jpg"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_16.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_17.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_18.jpg"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_19.jpg"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_20.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_21.jpg"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_22.jpg"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_23.jpg"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_24.jpg"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_logo_0.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_logo_1.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_logo_2.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_logo_3.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_logo_4.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_logo_5.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_logo_6.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_logo_7.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_logo_8.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_logo_9.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_logo_10.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_logo_11.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_logo_12.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_logo_13.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_logo_14.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_logo_15.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_logo_16.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_logo_17.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_logo_18.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_logo_19.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_logo_20.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_logo_21.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_logo_22.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_logo_23.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_logo_24.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_logo_25.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_logo_26.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_logo_27.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_logo_28.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_logo_29.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_logo_30.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_logo_31.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_logo_32.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_logo_33.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_logo_34.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_logo_35.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_logo_36.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_logo_37.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_logo_38.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_logo_39.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_logo_40.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_logo_41.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_logo_42.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_logo_43.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_logo_44.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_logo_45.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_logo_46.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_logo_47.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_logo_48.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_logo_49.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_logo_50.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_logo_51.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_logo_52.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_logo_53.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_logo_54.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_logo_55.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_logo_56.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_logo_57.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_logo_58.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_logo_59.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_logo_60.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_logo_61.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_logo_62.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_logo_63.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_logo_64.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_logo_65.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_logo_66.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_logo_67.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_logo_68.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_logo_69.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_logo_70.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_logo_71.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_logo_72.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_logo_73.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_logo_74.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_logo_75.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_logo_76.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_logo_77.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_logo_78.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_logo_79.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_logo_80.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_logo_81.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_logo_82.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_logo_83.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_logo_84.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_logo_85.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_logo_86.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_logo_87.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_logo_88.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_logo_89.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_logo_90.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_logo_91.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_logo_92.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_logo_93.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_logo_94.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_logo_95.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_logo_96.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_logo_97.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_logo_98.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_logo_99.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_logo_100.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_logo_101.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_logo_102.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_logo_103.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_logo_104.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_logo_105.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_logo_106.png"></span></li>
                        <li class="client-logo"><span class="logo-img"><img
                                    src="assets/images/clients/client_logo_107.png"></span></li>
                        </div>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <!--Plan-section end--> 



<!-- free-trial-section -->
<section class="free-trial-section wow fadeInUp">
	 <?php 
		include 'freeTrialSection.php';
		freeTrialSection("Want to See","Experienced and trusted by 1000+ schools across India. Request a demo to see how Schoollog can skyrocket your admissions!");
		?>
</section> 
<!--// free-trial-section --> 
<?php include 'footer.php';?>
<script type="text/javascript" src="assets/js/owl.carousel.min.js"></script>