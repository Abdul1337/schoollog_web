  
<!-- Footer Section--> 
<footer class="footer-section wow fadeInUp">
	<div class=container>
		<div class=row>
			
			<div class="col-md-6 wow fadeInLeft">
					<div class=office-outer>
						<div class="tab-content office-tabs-content">
							<div class="tab-pane active" id=office-tabs-1 role=tabpanel>
								<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3767.6145411941616!2d72.84170931490321!3d19.212030987009975!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3be7b6d11716f51b%3A0xa0f1fbcd02ed531b!2sSchoollog%20-%20App%20based%20School%20Management!5e0!3m2!1sen!2sin!4v1575354309377!5m2!1sen!2sin" class=office-map frameborder=0 style="border:0;" allowfullscreen=""></iframe>
							</div>
							<div class=tab-pane id=office-tabs-2 role=tabpanel>
								<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3559.1594285461356!2d75.71899551504391!3d26.86667538314745!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x396ca4ccffcf1379%3A0xf19f4c9751fc9734!2sSchoollog!5e0!3m2!1sen!2sin!4v1575354460057!5m2!1sen!2sin" class=office-map frameborder=0 style="border:0;" allowfullscreen=""></iframe>
							</div>
						</div>
						<ul class="nav nav-tabs office-tabs" role=tablist>
							<li class=nav-item>
								<a class="nav-link branch-btn active" data-toggle=tab href="#office-tabs-1" role=tab>Corprate Office</a>
							</li>
							<li class=nav-item>
								<a class="nav-link branch-btn" data-toggle=tab href="#office-tabs-2" role=tab>Branch Office</a>
							</li>
						</ul>
					</div>
				</div> 
			<div class="col-md-6 support-info wow fadeInRight">
				<div class="support-list">
					<div class="support-icon-col"> 
						<i class="icon-mobile"></i>
					</div>


					<div class="support-list-info clickable">
						<a href="tel:7023916106" style="color : white">
							<h4>
								Reach Out to Us!
							</h4>
							<p>
								+91 8448443326
							</p>
						</a>
					</div>
				</div>
				<div class="support-list">
					<div class=support-icon-col>
						<i class="icon-mail"></i>
					</div>
					<div class="support-list-info">
						<a href="mailto:info@schoollog.in" style="color : white">
							<h4>
								Mail to Us!
							</h4>
							<p>
								info@schoollog.in
							</p>
						</a>

					</div>
				</div>
				<div class="follow-col">
					<p> Follow Us </p>
					<ul>

						<li>
							<a href="https://www.facebook.com/schoollog" target="_blank">
								<span class="icon-fb2"></span>
							</a>
						</li>
						<li>
							<a href="https://twitter.com/schoolloginfo?lang=en" target="_blank">
								<span class="icon-twitter2"></span>
							</a>
						</li>
						<li>
							<a href="https://www.instagram.com/schoollog.in/" target="_blank">
								<span class="icon-instagram-logo2"></span>
							</a>
						</li>
						<li>
							<a href="https://www.youtube.com/channel/UCpDf-7sWHt6lJxwnB_iuOMA" target="_blank">
								<span class="icon-youtube2"></span>
							</a>
						</li>
						<li>
							<a href="https://wa.me/919785718555?text=Hi Schoollog!" target="_blank">
								<span class="icon-whatsapp2"></span>
							</a>
						</li>

					</ul>
				</div>
				<ul class="footer-nav">
					<li> <a href="terms_of_use.php"> Terms of Use </a> </li>
					<li> | </li>
					<li> <a href="privacy_policy.php"> Privacy Statement </a> </li>
					<li> | </li>
					<li> <a href="partnerus.php"> Partner Us </a> </li>

				</ul>
				<p class="copy-right-col"> © 2020 Schoollog. All rights reserved. </p>
			</div>
		</div>

	</div>

</footer> 
<!--//. Footer Section--> 

<!-- Return to Top -->
<a id="return-to-top" href="#" class="btn btn-light btn-lg back-to-top" role="button"><i class="icon-up-arrow"></i></a>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="http://code.jquery.com/jquery-3.4.1.min.js"></script>
<!--<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>-->
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/custom.js"></script> 
<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js">

</body>

</html>