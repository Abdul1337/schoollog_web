/*--Navigation function--*/
//(function($){"use strict";$(function(){var header=$(".start-style");$(window).scroll(function(){var scroll=$(window).scrollTop();if(scroll>=10){header.removeClass('start-style').addClass("scroll-on");}else{header.removeClass("scroll-on").addClass('start-style');}});});$('body').on('mouseenter mouseleave','.nav-item',function(e){if($(window).width()>750){var _d=$(e.target).closest('.nav-item');_d.addClass('show');setTimeout(function(){_d[_d.is(':hover')?'addClass':'removeClass']('show');},1);}});})(jQuery);

/*--Navigation function--*/
$('.dropdown-menu a.dropdown-toggle').on('click', function(e) {
 if (!$(this).next().hasClass('show')) {
	 $(this).parents('.dropdown-menu').first().find('.show').removeClass("show");
 }
 var $subMenu = $(this).next(".dropdown-menu");
 $subMenu.toggleClass('show');

 $(this).parents('li.nav-item.dropdown.show').on('hidden.bs.dropdown', function(e) {
	 $('.dropdown-submenu .show').removeClass("show");
 });
 return false;
});
 

/*--Back to top btn--*/
//$(document).ready(function(){$(window).scroll(function(){ if ($(this).scrollTop() > 200) {$('#backtop').fadeIn();} else { $('#backtop').fadeOut();}}); $('#backtop').click(function(){ $("html, body").animate({ scrollTop: 0 }, 600); return false; }); });

$(window).scroll(function() {
	 if ($(this).scrollTop() >= 50) { // If page is scrolled more than 50px
		 $('#return-to-top').fadeIn(200); // Fade in the arrow
	 } else {
		 $('#return-to-top').fadeOut(200); // Else fade out the arrow
	 }
});
$('#return-to-top').click(function() { // When arrow is clicked
	 $('body,html').animate({
		 scrollTop: 0 // Scroll to top of body
	 }, 500);
});

/*---scrolling header fixed function--*/
$(window).scroll(function() {
	 /* affix after scrolling 100px */
	 if ($(document).scrollTop() > 100) {
		 $('.header-classic').addClass('affix');
	 } else {
		 $('.header-classic').removeClass('affix');
	 }
 });


/*---Resize window function--*/

$(window).on('resize',function() {if ($(window).width() < 768) { var owl = $('.slide-carousel');owl.owlCarousel({touchDrag: true,mouseDrag: true,items:1,dots:true,autoHeight:false,margin: 20,nav:false,navText: ['<span aria-label="' + 'prev' + '"></span>','<span aria-label="' + 'next' + '"></span>'],});} else {}});$(document).ready(function() {$(window).trigger('resize');});

/*--Plan Page Hover effect function--*/
function highlight(divid){ $(".plan-first").removeClass("highlight"); $("#"+ divid).addClass("highlight"); } function remove_highlight(divid){$(".plan-first").removeClass("highlight"); $("#PREMIUM").addClass("highlight"); } 

/*--animation--*/
// wow = new WOW(  { animateClass: 'animated', offset: 200, callback: function(box) {console.log("WOW: animating <" + box.tagName.toLowerCase() + ">")}} );wow.init();

$(window).on('resize',function() {if ($(window).width()) { var owl = $('.slide-carousel2');owl.owlCarousel({touchDrag: true,mouseDrag: true, responsive:{ 0:{items:1, nav:true}, 800:{items:1, nav:false}, 1000:{items:2, nav:false, loop:false}},dots:true,autoHeight:false,margin: 20,nav:false,});} else {}});$(document).ready(function() {$(window).trigger('resize');});
 


//Testimonial slider
$(window).on('resize',function() {if ($(window).width()) { var owl = $('.testimonail-carousel');owl.owlCarousel({touchDrag: true,mouseDrag: true, responsive:{ 0:{items:1, nav:false}, 800:{items:1, nav:false}, 1000:{items:3, nav:false, loop:false}},dots:true,autoHeight:false,margin: 20, navText: ['<span aria-label="' + 'prev' + '"></span>','<span aria-label="' + 'next' + '"></span>'],});} else {}});$(document).ready(function() {$(window).trigger('resize');});


//Clietn Logo slider
$(window).on('resize',function() {if ($(window).width()) { var owl = $('.client-logo-carousel');owl.owlCarousel({touchDrag: true,mouseDrag: true, responsive:{ 0:{items:2, nav:false}, 800:{items:3, nav:false}, 1000:{items:4, nav:false, loop:false}},dots:false,autoHeight:false,margin: 20, navText: ['<span aria-label="' + 'prev' + '"></span>','<span aria-label="' + 'next' + '"></span>'],});} else {}});$(document).ready(function() {$(window).trigger('resize');});

// Home Page Form

// Home page form
$(".input-fields").css("transition", "all 0.4s ease-out")
var field = 1;
var numberOfFields = $(".inline-form-home").children().length;
$(".inline-form-home").css({
  "display": "block",
  "overflow-x": "hidden",
  "width": "90%",
  "white-space": "nowrap",
  "transition": "all 0.5s ease-out",
});
$(".btn-primary").on("click", e => {
  e.preventDefault();
  // console.log(e);
  if (field < numberOfFields) {
    var fields = $(".inline-form-home").children();
    let id = $(".inline-form-home").children()[field].id;
    if (validateInput($(".inline-form-home").children()[field - 1].id)) {
      for (let i = 0; i < numberOfFields; i++) {
        elm = $(".inline-form-home").children()[i].id;
        if($(window).width() > 765){
          $("#" + elm).css({
            "transform": "translateX(" + (-1 * field * (100)) + "%)",
          });
        }else{
          $("#" + elm).css({
            "transform": "translateY(" + (-1 * field * (100)) + "%)",
          });
        }
      }
      field += 1;
    }else{
      alert("Please fill the requested format")
    }
  } else {
    for (let i = numberOfFields; i > 0; i--) {
      elm = $(".inline-form-home").children()[i - 1].id;
      $("#" + elm).css({
        "transform": "translateX(0px)",
      });
    }
    field = 1;
    $("#signup").submit();
    $("#signup").trigger("reset");
  }
})
function validateInput(id) {
  let inputData = $("#" + id);
  return inputData[0].validity.valid;
}