<?php include 'header.php';?>

<div class="inner-page-header"> 
	<div class="full">
		<div class="inner-show-img"><img src="assets/images/partner_us.jpg"></div>
		<div class="container">
			<div class="slider-content">
				<h1>Partner Us</h1>
				<p class="subheading">India’s First AI Powered ERP For Schools</p>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="clearfix"></div>
</div>


<!-- //.Header Section -->

<!--Plan-section-->

<section class="counication-page partner-page">

	<div class="container">
		<div class="partner-section">
			<div class="partner-us-img">
				<img src="assets/images/partner_us_01.png">
			</div>
			<div class="partner-contant-section">
				<h2>Have connections in schools?</h2>
				<p>Let's do something great together then! Each partnership with Schoollog is
					unique and shared around priorities that contribute towards our goal of helping
					schools with their management needs. With this, the objective of Schoollog
					Channel partner program is to deliver a platform to all those people who want to
					expand their channels of financial growth.</p>
				<a href="contact_us.php" class="partner-event">Partner Events</a>
			</div>
		</div>

	</div>

	<div class="map-tab-bottom why-reasons">
		<div class="container">
			<div class="partner-section">
				<div class="partner-us-img">
					<h2>Schoollog Partner Plan</h2>
					<p>
						<b>Schoollog Partner Plan</b>
						allows you to resell the Services of Schoollog in your demography at competing
						prices. Schoollog helps its partners with the marketing tools to increase sales,
						time proven processes to help manage the sales effectively. Schoollog Partners
						get the benefit of a market validated erp with an elegant experience and timely
						support.
					</p>
				</div>
				<div class="key-contacts">
					<h3>Key Contacts</h3>
					<div class="key-contacts-link">
						<div class="user-image"><img src="assets/images/team_01.jpg"></div>
						<div class="user-detial">
							<p class="name-big">Abhishek Singh</p>
							<p>abhishek@techtalisman.com</p>
						</div>
					</div>

					<div class="key-contacts-link">
						<div class="user-image"><img src="assets/images/team_04.png"></div>
						<div class="user-detial">
							<p class="name-big">Sachin Bakshi</p>
							<p>sachin@techtalisman.com</p>
						</div>
					</div>
				</div>
			</div>

		</div>

		<!-- <div class="panel-group" id="accordion" role="tablist"
            aria-multiselectable="true"></div> -->
	</div>

	<div class="map-tab-bottom india-map">
		<div class="container">
			<div class="awesome-app-list-tabs-col">
				<div class="awesome-app-list-content-col">
					<div class="tab-content awesome-app-list-content india_map">
						<div class="tab-pane active" id="admin-app-list-tabs-1" role="tabpanel">
							<img src="assets/images/schoollog_india.png" />
						</div>
						<div class="tab-pane" id="admin-app-list-tabs-2" role="tabpanel">
							<img src="assets/images/schoollog_india.png" />
						</div>
						<div class="tab-pane" id="admin-app-list-tabs-3" role="tabpanel">
							<img src="assets/images/schoollog_india.png" />
						</div>
						<div class="tab-pane" id="admin-app-list-tabs-4" role="tabpanel">
							<img src="assets/images/schoollog_india.png" />
						</div>
						<div class="tab-pane" id="admin-app-list-tabs-5" role="tabpanel">
							<img src="assets/images/schoollog_india.png" />
						</div>
						<div class="tab-pane" id="admin-app-list-tabs-6" role="tabpanel">
							<img src="assets/images/schoollog_india.png" />
						</div>
					</div>
				</div>
				<ul class="nav nav-tabs awesome-app-list-tabs" role="tablist" style="margin-top:0px">
					<li class="nav-item">
						<a class="nav-link active" data-toggle="tab" href="#admin-app-list-tabs-1" role="tab">
							<div class="awesome-app-list-nav-link" class="map-tab-partner-us-buttons">
								<span class="grow-tab-icon">
									<i class="icon-map_tab_02"></i>
								</span>

								<p>
									Established Brand
								</p>
							</div>
						</a>
					</li>

					<li class="nav-item">
						<a class="nav-link" data-toggle="tab" href="#admin-app-list-tabs-2" role="tab">
							<div class="awesome-app-list-nav-link" class="map-tab-partner-us-buttons">
								<span class="grow-tab-icon">
									<i class="icon-user"></i>

								</span>
								<p>
									Replicable Know How
								</p>
							</div>
						</a>
					</li>

					<li class="nav-item">
						<a class="nav-link" data-toggle="tab" href="#admin-app-list-tabs-3" role="tab">
							<div class="awesome-app-list-nav-link" class="map-tab-partner-us-buttons">
								<span class="grow-tab-icon">
									<i class="icon-map_tab_04"></i>
								</span>

								<p>
									Marketing Support</p>
							</div>
						</a>
					</li>

					<li class="nav-item">
						<a class="nav-link" data-toggle="tab" href="#admin-app-list-tabs-4" role="tab">
							<div class="awesome-app-list-nav-link" class="map-tab-partner-us-buttons">
								<span class="grow-tab-icon">
									<i class="icon-map_tab_03"></i>

								</span>
								<p>
									One Time Investment
								</p>
							</div>
						</a>
					</li>

					<li class="nav-item">
						<a class="nav-link" data-toggle="tab" href="#admin-app-list-tabs-5" role="tab">
							<div class="awesome-app-list-nav-link" class="map-tab-partner-us-buttons">
								<span class="grow-tab-icon">

									<i class="icon-map_tab_05"></i>
								</span>
								<p>
									Dedicated Franchise Manager</p>
							</div>
						</a>
					</li>

					<li class="nav-item">
						<a class="nav-link" data-toggle="tab" href="#admin-app-list-tabs-6" role="tab">
							<div class="awesome-app-list-nav-link" class="map-tab-partner-us-buttons">
								<span class="grow-tab-icon">

									<i class="icon-calendar_001"></i>

								</span>
								<p>
									Global Presence</p>
							</div>
						</a>
					</li>

				</ul>
				<!-- Tab panes -->
			</div>
		</div>
	</div>

	<div class="map-tab-bottom why-reasons partern-with-us-last">
		<div class="container">
			<h2>Partner with us</h2>
			<p>Enjoy the Benefits of Perpetual Recurring Exponential Income</p>
			<ul class="reasons-reuired">
				<div class="slide-carousel">
					<li>
						<span class="counting-01">1</span>
						<span class="list-title">Getting Started</span>
						<span class="small-text">Make the Payment</span>
					</li>

					<li>
						<span class="counting-01">2</span>
						<span class="list-title">Sign the Agreement</span>
						<span class="small-text">Seal the deal with the Agreement</span>
					</li>

					<li>
						<span class="counting-01">3</span>
						<span class="list-title">Onboarding</span>
						<span class="small-text">Orientation & In-depth Training Program</span>
					</li>

					<li>
						<span class="counting-01">4</span>
						<span class="list-title">Make Money</span>
						<span class="small-text">Enjoy-Perpetual Recurring Income</span>
					</li>
				</div>

			</ul>
		</div>

	</div>
</section>
<!--Plan-section end-->




<!-- free-trial-section -->
<section class="free-trial-section wow fadeInUp">
	<?php 
		include 'freeTrialSection.php';
		freeTrialSection("Want to See","How to save time, reduce your workload<br/>and enhance learning?");
		?>
</section>
<!--// free-trial-section -->
<?php include 'footer.php';?>
<script type="text/javascript" src="assets/js/owl.carousel.min.js"></script>