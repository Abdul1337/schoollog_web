<!doctype html>
<html lang="en">

<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="shortcut icon" href="assets/images/favicon.ico">
	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<link href="assets/css/navigation.css" rel="stylesheet" />
 	<link href="assets/css/svg_style.css" rel="stylesheet" />	
	<link href="assets/css/custom.css" rel="stylesheet" />
	<link href="assets/css/developer.css" rel="stylesheet" />
	<link href="assets/css/custom-responsive.css" rel="stylesheet" />
	
	<title>Schoollog | School ERP & LMS</title>
</head> 
<body>
	<!-- header start -->
	<div class="header-classic">
		<!-- navigation start -->
		<div class="container">
			<div class="row">
				<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
					<nav class="navbar navbar-expand-lg navbar-classic">
						<a class="navbar-brand" href="index.php"> <img src="assets/images/logo.png" alt="" /> </a>
						<button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbar-classic" aria-controls="navbar-classic" aria-expanded="false" aria-label="Toggle navigation">
							<span class="icon-bar top-bar mt-0"></span>
							<span class="icon-bar middle-bar"></span>
							<span class="icon-bar bottom-bar"></span>
						</button>
						<div class="collapse navbar-collapse" id="navbar-classic">
							<ul class="navbar-nav ml-auto mt-2 mt-lg-0 mr-3">
								<li class="nav-item">
									<a class="nav-link active" href="index.php">Home</a>
								</li>

								<li class="nav-item dropdown">
									<a class="nav-link dropdown-toggle" href="whyschoollog.php" id="menu-1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										Why Schoollog
									</a>
									<ul class="dropdown-menu" aria-labelledby="menu-1">
										<li><a class="dropdown-item" href="reasons_list.php">100 Reasons Why</a> </li>
										<li><a class="dropdown-item" href="whyschoollog.php">Why Schoollog</a></li>
										<li><a class="dropdown-item" href="plan.php">Plans</a> </li>
									</ul>
								</li>

								<li class="nav-item dropdown">
									<a class="nav-link dropdown-toggle" href="#" id="menu-1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										About Us
									</a>
									<ul class="dropdown-menu" aria-labelledby="menu-1">

										<li> <a class="dropdown-item" href="life_of_schoollog.php">Life@Schoollog</a> </li>
										<li> <a class="dropdown-item" href="our_team.php"> Our Team </a> </li>

									</ul>
								</li>

								<li class="nav-item">
									<a class="nav-link" href="client.php">Clients</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" href="partnerus.php">Partner Us</a>
								</li>

								<li class="nav-item dropdown">
									<a class="nav-link dropdown-toggle" href="#" id="menu-1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										Modules
									</a>
									<ul class="dropdown-menu" aria-labelledby="menu-1">
										<li>
											<a class="dropdown-item" href="comunication.php">Communication</a>
										</li>

										<li> <a class="dropdown-item" href="exams.php">Exams</a> </li>
										<li> <a class="dropdown-item" href="administration.php">Administration</a> </li>
										<li> <a class="dropdown-item" href="enrollment.php">Enrollment</a> </li>
										<li> <a class="dropdown-item" href="smart_attendance.php">Smart Attendance</a> </li>
										<li> <a class="dropdown-item" href="inventory.php">Inventory</a> </li>

									</ul>
								</li>

								<li class="nav-item dropdown">
									<a class="nav-link dropdown-toggle" href="#" id="menu-1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										More
									</a>
									<ul class="dropdown-menu" aria-labelledby="menu-1">

										<li> <a class="dropdown-item" href="contact_us.php">Contact Us</a> </li>
										<li> <a class="dropdown-item" href="files\school_brochure.pdf" download>Download Brochure</a> </li>
										<li> <a class="dropdown-item" href="download_apps.php" target="_blank">Download Our Apps</a> </li>
										<li> <a class="dropdown-item" href="faq.php">FAQ</a> </li>
										<li> <a class="dropdown-item" href="https://blog.schoollog.in/" target="_blank">Blog</a> </li>
										<li> <a class="dropdown-item" href="https://demo.schoollog.in/" target="_blank">Demo Website</a> </li>
										<li> <a class="dropdown-item" href="terms_conditions.php">Terms And Conditions</a> </li>
										<li> <a class="dropdown-item" href="privacy_policy.php">Privacy Policy</a> </li>
										<li> <a class="dropdown-item" href="terms_of_use.php">Terms Of Use</a> </li>

									</ul>
								</li>

								<li class="nav-item">
									<a class="nav-link  login-bgn" href="https://erp.schoollog.in" target="_blank">Login</a>
								</li>
							</ul>
							<a class="btn btn-brand btn-rounded btn-sm" href="https://schoollog.in/landingpage/" target="_blank">Request Free Demo</a>
						</div>
					</nav>
				</div>
			</div>
		</div>
		<!-- navigation close -->
	</div>
	<!-- header close -->
	 