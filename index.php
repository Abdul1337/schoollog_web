 <?php include 'header.php';?>
 
 	<!-- Main Banner-->
 	<section class="main-banner-section" id="section01">
 		<a class="scroll-next" href="#section02"><span></span><span></span><span></span>Scroll</a>
 		<div class=container>

 			<div class="home-main-banner wow fadeInDown">
 				<h5 class=main-banner-heading> SCHOOLLOG <span> everything <br> </span> </h5>
				 <h4 class=main-banner-subheading>that your school needs! </h4>
				 
 				<!-- <form class="form-inline form-banner">
 					<div class=form-group>
 						<label for=email class=sr-only>Password</label>
 						<input type=email class=form-control id=inputPassword2 placeholder="What’s your name?">
 					</div>
 					<button type=submit class="btn btn-primary"> <img src="assets/images/go_icon.png" /> </button>
 				</form> -->
				 <form class="form-inline form-banner" id="signup">
					<div class="form-group inline-form-home">
						<input name="name" type="text" class="form-control input-fields" id="input-name" placeholder="What’s your name?" required autocomplete="off">
						<input name="email" type="email" class="form-control input-fields" id="input-email" placeholder="What’s your email?" required autocomplete="off">
						<input name="mobile" type="text" pattern="[0-9]{10}" class="form-control input-fields" id="input-pnone" placeholder="What’s your Phone Number?" required autocomplete="off">
						<input name="msg" type="text" class="form-control input-fields" id="input-message" placeholder="any message for us?" required autocomplete="off">
					</div>
					<button class="btn btn-primary"><img src="assets/images/go_icon.png" /></button>
				</form>
 				<div id=carousel-example-2 class="carousel slide mb-5 banner-slide" data-ride=carousel>
 					<div class=carousel-inner role=listbox>
 						<div class="carousel-item active">
 							<div class="view hm-white-slight">
 								<img src="assets/images/boost_admissions_icon.png" alt="Boost slide">
 								<h3> Boost Admissions </h3>
 								<p> 1000 's </p>
 							</div>
 						</div>
 						<div class=carousel-item>
 							<div class="view hm-white-slight">
 								<img src="assets/images/energize_communication_icon.png" alt="Energize slide">
 								<h3> Energize communication </h3>
 								<p> 50% </p>
 							</div>
 						</div>
 						<div class=carousel-item>
 							<div class="view hm-white-slight">
 								<img src="assets/images/admissions_icon.png" alt="Admissions slide">
 								<h3> Admissions </h3>
 								<p> 100% </p>
 							</div>
 						</div>
 					</div>
 					<a class=carousel-control-prev href="#carousel-example-2" role=button data-slide=prev>
 						<span class=carousel-control-prev-icon aria-hidden=true></span>
 						<span class=sr-only>Previous</span>
 					</a>
 					<a class=carousel-control-next href="#carousel-example-2" role=button data-slide=next>
 						<span class=carousel-control-next-icon aria-hidden=true></span>
 						<span class=sr-only>Next</span>
 					</a>
 				</div>
 			</div>

 		</div>
 	</section>
 	<!--//.Main Banner-->

 	<!-- Section 01 -->
 	<section class="headache-section" id="section02"> 
 		<a class="scroll-next" href="#section03"><span></span><span></span><span></span>Scroll</a>
 		<div class=container>
 			<div class=row>
 				<div class="col-md-6 headache-col wow fadeInLeft">
 					<h4 class=headache-col-heading> Is managing a school becoming headache? </h4>

 					<h5 class=headache-col-subheading> TRADITIONAL APPROACH </h5>

 					<div class=headache-chart>
 						<img src="assets/images/traditional_approach.png" alt="" />
 					</div>
 				</div>
 				<div class="col-md-6 headache-col schoollog-way wow fadeInRight">
 					<h4 class=headache-col-heading> Let us make it simple for you. </h4>

 					<h5 class=headache-col-subheading> SCHOOLLOG WAY</h5>

 					<div class=headache-chart>
 						<img src="assets/images/schoollog_way.png" alt="" />
 					</div>
 				</div>
 			</div>

 		</div>
 	</section>
 	<!-- //.Section 01 -->

 	<!-- //.Section 02 -->
 	<section class="grow-section wow fadeInUp" id="section03">
 		<a class="scroll-next" href="#section04"><span></span><span></span><span></span>Scroll</a>
 		<div class=container>
 			<ul class="nav nav-tabs grow-tabs wow fadeInLeft" role=tablist>
 				<li class=nav-item>
 					<a class="nav-link active" data-toggle=tab href="#tabs-1" role=tab>
 						<i class="icon-grow_icon01"></i> Grow Student
 					</a>
 				</li>
 				<li class=nav-item>
 					<a class="nav-link " data-toggle=tab href="#tabs-2" role=tab>
 						<i class="icon-grow_icon02"></i> Communication</a>
 				</li>	
 				<li class=nav-item>
 					<a class="nav-link grpw-tab-icon2" data-toggle=tab href="#tabs-3" role=tab>
 						<i class="icon-grow_icon03"></i> Exams</a>
 				</li>
 				<li class=nav-item>
 					<a class="nav-link " data-toggle=tab href="#tabs-4" role=tab>
 						<i class="icon-grow_icon04"></i> Smart Attendance</a>
 				</li>
 				<li class=nav-item>
 					<a class="nav-link " data-toggle=tab href="#tabs-5" role=tab>
 						<i class="icon-grow_icon05"></i> Fee Management</a>
 				</li>
 				<li class=nav-item>
 					<a class="nav-link " data-toggle=tab href="#tabs-6" role=tab>
 						<i class="icon-grow_icon06"></i> Transport </a>
 				</li>
 				<li class=nav-item>
 					<a class="nav-link " data-toggle=tab href="#tabs-7" role=tab>
 						<i class="icon-grow_icon07"></i> Hostel Management</a>
 				</li>
 				<li class=nav-item>
 					<a class="nav-link " data-toggle=tab href="#tabs-8" role=tab>
 						<i class="icon-grow_icon08"></i> Inventory</a>
 				</li>
 				<li class="nav-item add-on-tab">
 					<a class="nav-link " data-toggle=tab href="#tabs-9" role=tab>
 						<span class=grow-tab-icon> + </span>
 						<p> Add On </p>
 					</a>
 				</li>
 			</ul>
 			<div class="tab-content grow-tab-content wow fadeInRight">
 				<div class="tab-pane tab-grow active" id=tabs-1 role=tabpane2>
 					<div class=grow-tab-content-chart> <img src="assets/images/grow_student_chart.png" /> </div>
 					<h5> Grow Student </h5>
 					<div class=scrollbar id=style-1>
 						<div class=force-overflow>
 							<ul>
 								<li>
 									<h6> <span class=force-overflow-icon>
 											<i class="icon-icon_01"></i>	
 										</span> Enquiry Management : </h6>
 									<p> Focus exclusively on growing students by skipping all the admission hassle. All
 										you need to do is fill the student information and Schoollog does the rest for
 										you. </p>
 								</li>
 								<li>
 									<h6> <span class=force-overflow-icon>
 											<i class="icon-icon_02"></i>	
 										</span> Follow-ups : </h6>
 									<p> Schoollog never lets you miss any potential lead. Stay in touch with all your
 										enquiries by sending follow-up messages with a single click </p>
 								</li>
 								<li>
 									<h6> <span class=force-overflow-icon>
 											<i class="icon-icon_03"></i>
 										</span> Paperless : </h6>
 									<p> Schoollog helps you manage all your enquiries at a single place and make sure
 										you get the most out of a potential lead in a little period of time, no more
 										pen-paper hustling. </p>
 								</li>
 								<li>
 									<h6> <span class=force-overflow-icon>
 											<i class="icon-icon_04"></i>
 										</span> Ready-made Analytics : </h6>
 									<p> No more brainstorming sessions, we have done it for you. Get insight into all
 										the statistics and prepare your best strategies. </p>
 								</li>
 							</ul>
 						</div>
 					</div>
 				</div>
 				<div class="tab-pane tab-grow" id=tabs-2 role=tabpane2>
 					<div class=grow-tab-content-chart> <img src="assets/images/communication_chart.png" /> </div>
 					<h5> Communication </h5>
 					<div class=scrollbar id=style-1>
 						<div class=force-overflow>
 							<ul>
 								<li>
 									<h6> <span class=force-overflow-icon>
 											<i class="icon-parent_app_icon02"></i>
 										</span> Instant notification : </h6>
 									<p> Don’t let your parents and staff forget you. Keep them updated with all the
 										latest information about everything taking place in school. Schoollog lets you
 										send unlimited in-app notifications without any extra charge. </p>
 								</li>
 								<li>
 									<h6> <span class=force-overflow-icon>
 											<i class="icon-icon_08"></i>
 										</span> SMS notifications : </h6>
 									<p> Send an SMS for emergency notifications, parenting meetings, student attendance
 										etc. right within the Schoollog panel itself. </p>
 								</li>
 								<li>
 									<h6> <span class=force-overflow-icon>
 											<i class="icon-icon_07"></i>
 										</span> Digital diary : </h6>
 									<p> With Schoollog send students' homework and assignment updates directly to
 										parents. Let the teacher be free of worries of a child hiding homework from
 										parents. </p>
 								</li>
 								<li>
 									<h6> <span class=force-overflow-icon>
 											<i class="icon-icon_09"></i>
 										</span> Fee status alerts : </h6>
 									<p> Send regular fee due alerts to parents and get your fee collection done on time.
 									</p>
 								</li>
 								<li>
 									<h6> <span class=force-overflow-icon>
 											<i class="icon-icon_10"></i>
 										</span> Calendar : </h6>
 									<p> Pre-schedule all the important events and holidays to keep the parents updated
 										in advance. </p>
 								</li>
 							</ul>
 						</div>
 					</div>
 				</div>
 				<div class="tab-pane tab-grow" id=tabs-3 role=tabpane2>
 					<div class=grow-tab-content-chart> <img src="assets/images/exams_chart.png" /> </div>
 					<h5> Exams </h5>
 					<p class=grow-tab-content-sub-div>
 					</p>
 					<div class=scrollbar id=style-1>
 						<div class=force-overflow>
 							<ul>
 								<li>
 									<p> We know it's hard to believe but yes you can structure your exams within 10 mins with Schoollog. </p>
 								</li>
 								<li>
 									<h6> <span class=force-overflow-icon>
 											<i class="icon-icon_11"></i>
 										</span> Gradebook : </h6>
 									<p> Focus exclusively on growing students by skipping all the admission hassle. All
 										you need to do is fill the student information and Schoollog does the rest for
 										you </p>
 								</li>
 								<li>
 									<h6> <span class=force-overflow-icon>
 											<i class="icon-parent_app_icon02"></i>
 										</span> Exam alerts : </h6>
 									<p> Share exam schedule information and attachments with students & teachers. Send
 										to entire school, class, group or student. </p>
 								</li>
 								<li>
 									<h6> <span class=force-overflow-icon>
 											<i class="icon-icon_11"></i>
 										</span> Admit Card : </h6>
 									<p> Print admit card of all students at once with ease and minimal effort. Just a
 										click away! </p>
 								</li>
 								<li>
 									<h6> <span class=force-overflow-icon>
 											<i class="icon-icon_04"></i>
 										</span> Scholastic and Co-scholastic
 										performance : </h6>
 									<p> Determine student’s academic and co-curricular overall performance at one go,
 										with all the required data pre-populated from various tests, assignments and
 										skills. </p>
 								</li>
 								<li>
 									<h6> <span class=force-overflow-icon>
 											<i class="icon-icon_12"></i>
 										</span> Result Analysis : </h6>
 									<p> Focus exclusively on growing students by skipping all the admission hassle. All
 										you need to do is fill the student information and Schoollog does the rest for
 										you. </p>
 								</li>
 							</ul>
 						</div>
 					</div>
 				</div>
 				<div class="tab-pane tab-grow" id=tabs-4 role=tabpane2>
 					<div class=grow-tab-content-chart> <img src="assets/images/smart_attendance_chart.png" /> </div>
 					<h5> Smart Attendance </h5>
 					<div class=scrollbar id=style-1>
 						<div class=force-overflow>
 							<ul>
 								<li>
 									<h6> <span class=force-overflow-icon>
 											<i class="icon-parent_app_icon02"></i>
 										</span> Realtime attendance alert : </h6>
 									<p> Make parents free of worries of a child reaching school or not. With Schoollog,
 										send daily real-time attendance updates to parents. </p>
 								</li>
 								<li>
 									<h6> <span class=force-overflow-icon>
 											<i class="icon-icon_05"></i>
 										</span> Biometric integration : </h6>
 									<p> With Schoollog teacher app, record attendance manually in no time or through
 										biometric device integration. </p>
 								</li>
 								<li>
 									<h6> <span class=force-overflow-icon>
 											<i class="icon-parent_app_icon04"></i>
 										</span> Weekly/monthly reports : </h6>
 									<p> Want to check month old records? We have got you covered! Generate
 										weekly/monthly attendance report for a single student, class or whole school on
 										a click. </p>
 								</li>
 								<li>
 									<h6> <span class=force-overflow-icon>
 											<i class="icon-icon_06"></i>
 										</span> Leave management : </h6>
 									<p> Focus exclusively on growing students by skipping all the admission hassle. All
 										you need to do is fill the student information and Schoollog does the rest for
 										you. </p>
 								</li>
 							</ul>
 						</div>
 					</div>
 				</div>
 				<div class="tab-pane tab-grow" id=tabs-5 role=tabpane2>
 					<div class=grow-tab-content-chart> <img src="assets/images/fee_management_chart.png" /> </div>
 					<h5> Fee management </h5>
 					<div class=scrollbar id=style-1>
 						<div class=force-overflow>
 							<ul>
 								<li>
 									<p> Schoollog helps you with managing all your fee management chores under one roof with keeping all
 										the records safe and secure. </p>
 								</li>
 								<li>
 									<h6> <span class=force-overflow-icon>
 											<i class="icon-icon_06"></i>
 										</span> Fee collection reports : </h6>
 									<p> Don't have time to read pages long Accounts report? With Schoollog, get daily
 										fee collection updates directly on your phone. </p>
 								</li>
 								<li>
 									<h6> <span class=force-overflow-icon>
 											<i class="icon-parent_app_icon04"></i></span> Defaulters report : </h6>
 									<p> Get defaulter report on a go and schedule timely reminders to parents. </p>
 								</li>
 								<li>
 									<h6> <span class=force-overflow-icon>
 											<i class="icon-icon_16"></i>
 										</span> Bulk fine/concession : </h6>
 									<p> Assigning fine/concession to multiple people in a go and save all the extra
 										time. </p>
 								</li>
 								<li>
 									<h6> <span class=force-overflow-icon>
 											<i class="icon-icon_15"></i>
 										</span> Readymade ledger : </h6>
 									<p> Export a detailed or consolidated ledger report whenever your want. </p>
 								</li>
 								<li>
 									<h6> <span class=force-overflow-icon>
 											<i class="icon-icon_14"></i>
 										</span> Multiple Accounts profiles : </h6>
 									<p>Give specific controlled access to different users that oversee school accounts.
 									</p>
 								</li>
 							</ul>
 						</div>
 					</div>
 				</div>
 				<div class="tab-pane tab-grow" id=tabs-6 role=tabpane2>
 					<div class=grow-tab-content-chart> <img src="assets/images/transport_chart.png" /> </div>
 					<h5> Transport </h5>
 					<div class=scrollbar id=style-1>
 						<div class=force-overflow>
 							<ul>
 								<li>
 									<p> Get rid of collecting and maintaining transport fee separately. Collect the transport fee along
 										with the academic fee collection and get all the records done automatically. </p>
 								</li>
 								<li>
 									<h6> <span class=force-overflow-icon>
 											<i class="icon-icon_17"></i>
 										</span> Vehicle-Driver management : </h6>
 									<p> Manage all your drivers and vehicles details in a single place thus making
 										transport record management easier and effortless. </p>
 								</li>
 								<li>
 									<h6> <span class=force-overflow-icon>
 											<i class="icon-icon_18"></i>
 										</span> Map stops and routes with students :
 									</h6>
 									<p> Allot stops and routes to students and manage transport fee collection
 										effectively. </p>
 								</li>
 							</ul>
 						</div>
 					</div>
 				</div>
 				<div class="tab-pane tab-grow" id=tabs-7 role=tabpane2>
 					<div class=grow-tab-content-chart> <img src="assets/images/hostel_management_chart.png" /> </div>
 					<h5> Hostel management </h5>
 					<p> Define hostel floors, rooms and beds. Map students and manage hostel fee within the same
 						accounts. Generate reports like hostel student list, hostel fee dues and refund reports, etc
 						whenever you want. </p>
 				</div>
 				<div class="tab-pane tab-grow" id=tabs-8 role=tabpane2>
 					<div class=grow-tab-content-chart> <img src="assets/images/inventory_chart.png" /> </div>
 					<h5> Inventory </h5>
 					<p> Schoollog gives you an easy to use stock management system for your school assisting you with
 						supervising and assuring of the ordering, storage, and use of components. </p>
 				</div>
 				<div class="tab-pane tab-grpw" id=tabs-9 role=tabpane2>
 					<div class=grow-tab-content-chart> <img src="assets/images/add_ons_chart.png" /> </div>
 					<h5>Add ons </h5>
 					<div class=scrollbar id=style-1>
 						<div class=force-overflow>
 							<ul>
 								<li>
 									<h6> <span class=force-overflow-icon>
 											<i class="icon-icon_23"></i>
 										</span> HR and payroll : </h6>
 									<p> Schoollog provides hassle-free processing of employees salary generation. It's a
 										fully integrated solution with attendance and leave management system allowing
 										for seamless movement of absence and loss of pay data. </p>
 								</li>
 								<li>
 									<h6> <span class=force-overflow-icon>
 											<i class="icon-icon_22"></i>
 										</span> Expense management : </h6>
 									<p> Need to sort out information on where your money goes? Throw away the
 										conventional budget planning sheets like Excel! Link your bank accounts to
 										Schoollog and automatically manage and track incoming/outgoing transactions.
 									</p>
 								</li>
 								<li>
 									<h6> <span class=force-overflow-icon>
 											<i class="icon-icon_19"></i>
 										</span> Student wallet : </h6>
 									<p> Schoollog wallet helps reduces cash misuse in the school with Real-time
 										visibility of students expenses. </p>
 								</li>
 								<li>
 									<h6> <span class=force-overflow-icon>
 											<i class="icon-icon_21"></i>
 										</span> Certificate generation : </h6>
 									<p> Generate certificates like studying certificate, Transfer certificate, Fee
 										certificate, Character certificate etc in a click. </p>
 								</li>
 								<li>
 									<h6> <span class=force-overflow-icon>
 											<i class="icon-icon_20"></i>
 										</span> Website management console : </h6>
 									<p> Keep your website up to date with the latest school's news/ announcements/
 										events pictures & videos and much more with the help of Schoollog Website
 										administration console. </p>
 								</li>
 							</ul>
 						</div>
 					</div>
 				</div>
 			</div>
 			<div class=panel-group id=accordion role=tablist aria-multiselectable=true></div>
 		</div>
 	</section>
 	<!-- //.Section 02 -->

 	<!-- Section 03 -->
 	<section class="awesome-app-section wow fadeInUp" id="section04">
 		<a class="scroll-next" href="#section05"><span></span><span></span><span></span>Scroll</a>
 		<div class=container>
 			<h4 class=awesome-app-title> The Awesome App </h4>
 			<ul class="nav nav-tabs awesome-app-tabs wow fadeInLeft" role=tablist>
 				<li class=nav-item>
 					<a class="nav-link active" data-toggle=tab href="#awesome-app-tabs-1" role=tab>
 						Parent <br /> App

 						<span> </span>
 					</a>
 				</li>
 				<li class=nav-item>
 					<a class=nav-link data-toggle=tab href="#awesome-app-tabs-2" role=tab>
 						Teacher <br /> App

 						<span> </span>
 					</a>
 				</li>
 				<li class=nav-item>
 					<a class=nav-link data-toggle=tab href="#awesome-app-tabs-3" role=tab>
 						Director <br /> App

 						<span> </span>
 					</a>
 				</li>
 				<li class=nav-item>
 					<a class=nav-link data-toggle=tab href="#awesome-app-tabs-4" role=tab>
 						Principal <br /> App

 						<span> </span>
 					</a>
 				</li>
 				<li class=nav-item>
 					<a class=nav-link data-toggle=tab href="#awesome-app-tabs-5" role=tab>
 						Admin <br /> App

 						<span> </span>
 					</a>
 				</li>
 			</ul>
 			<div class="tab-content awesome-app-content">
 				<div class="tab-pane active" id=awesome-app-tabs-1 role=tabpanel>
 					<div class=awesome-app-content-outer>
 						<div class=awesome-app-list-tabs-col>
 							<ul class="nav nav-tabs awesome-app-list-tabs" role=tablist>
 								<li class=nav-item>
 									<a class="nav-link active" data-toggle=tab href="#parent-app-list-tabs-1" role=tab>
 										<div class=awesome-app-list-nav-link>
 											<span class=grow-tab-icon>
 												<i class="icon-parent_app_icon01"></i>
 											</span>
 											<p> Communication </p>
 										</div>
 									</a>
 								</li>
 								<li class=nav-item>
 									<a class=nav-link data-toggle=tab href="#parent-app-list-tabs-2" role=tab>
 										<div class=awesome-app-list-nav-link>
 											<span class=grow-tab-icon>
 												<i class="icon-parent_app_icon02"></i>
 											</span>
 											<p> Push notification </p>
 										</div>
 									</a>
 								</li>
 								<li class=nav-item>
 									<a class=nav-link data-toggle=tab href="#parent-app-list-tabs-3" role=tab>
 										<div class=awesome-app-list-nav-link>
 											<span class=grow-tab-icon>
 												<i class="icon-parent_app_icon03"></i>
 											</span>
 											<p> Exam results </p>
 										</div>
 									</a>
 								</li>
 								<li class=nav-item>
 									<a class=nav-link data-toggle=tab href="#parent-app-list-tabs-4" role=tab>
 										<div class=awesome-app-list-nav-link>
 											<span class=grow-tab-icon>
 												<i class="icon-parent_app_icon04"></i>
 											</span>
 											<p> Leave application </p>
 										</div>
 									</a>
 								</li>
 								<li class=nav-item>
 									<a class=nav-link data-toggle=tab href="#parent-app-list-tabs-5" role=tab>
 										<div class=awesome-app-list-nav-link>
 											<span class=grow-tab-icon>
 												<i class="icon-parent_app_icon05"></i>
 											</span>
 											<p> Attendance notification </p>
 										</div>
 									</a>
 								</li>
 								<li class=nav-item>
 									<a class=nav-link data-toggle=tab href="#parent-app-list-tabs-6" role=tab>
 										<div class=awesome-app-list-nav-link>
 											<span class=grow-tab-icon>
 												<i class="icon-parent_app_icon06"></i>
 											</span>
 											<p> Exam schedule </p>
 										</div>
 									</a>
 								</li>
 								<li class=nav-item>
 									<a class=nav-link data-toggle=tab href="#parent-app-list-tabs-7" role=tab>
 										<div class=awesome-app-list-nav-link>
 											<span class=grow-tab-icon>
 												<i class="icon-parent_app_icon07"></i>
 											</span>
 											<p> Homework updates </p>
 										</div>
 									</a>
 								</li>
 								<li class=nav-item>
 									<a class=nav-link data-toggle=tab href="#parent-app-list-tabs-8" role=tab>
 										<div class=awesome-app-list-nav-link>
 											<span class=grow-tab-icon>
 												<i class="icon-parent_app_icon08"></i>
 											</span>
 											<p> Fee & Online Payment </p>
 										</div>
 									</a>
 								</li>
 								<li class=nav-item>
 									<a class=nav-link data-toggle=tab href="#parent-app-list-tabs-9" role=tab>
 										<div class=awesome-app-list-nav-link>
 											<span class=grow-tab-icon>
 												<i class="icon-parent_app_icon09"></i>
 											</span>
 											<p> Events/Holidays updates </p>
 										</div>
 									</a>
 								</li>
 								<li class=nav-item>
 									<a class=nav-link data-toggle=tab href="#parent-app-list-tabs-10" role=tab>
 										<div class=awesome-app-list-nav-link>
 											<span class=grow-tab-icon>
 												<i class="icon-parent_app_icon10"></i>
 											</span>
 											<p> School gallery </p>
 										</div>
 									</a>
 								</li>
 							</ul>
 						</div>
 						<div class=awesome-app-list-content-col>
 							<div class="tab-content awesome-app-list-content wow fadeInRight">
 								<div class="tab-pane active" id=parent-app-list-tabs-1 role=tabpanel>
 									<h4>Communication</h4>
 									<img src="assets/images/awesome-app/communication.png" />
 								</div>
 								<div class=tab-pane id=parent-app-list-tabs-2 role=tabpanel>
 									<h4>Push notification</h4>
 									<img src="assets/images/awesome-app/push-notification.png" />
 								</div>
 								<div class=tab-pane id=parent-app-list-tabs-3 role=tabpanel>
 									<h4>Exam results</h4>
 									<img src="assets/images/awesome-app/share-exam-results.png" />
 								</div>
 								<div class=tab-pane id=parent-app-list-tabs-4 role=tabpanel>
 									<h4>Leave application</h4>
 									<img src="assets/images/awesome-app/leave-application.png" />
 								</div>
 								<div class=tab-pane id=parent-app-list-tabs-5 role=tabpanel>
 									<h4>Attendance notification</h4>
 									<img src="assets/images/awesome-app/attendance-notification.png" />
 								</div>
 								<div class=tab-pane id=parent-app-list-tabs-6 role=tabpanel>
 									<h4>Exam schedule</h4>
 									<img src="assets/images/awesome-app/exam-schedule.png" />
 								</div>
 								<div class=tab-pane id=parent-app-list-tabs-7 role=tabpanel>
 									<h4>Homework updates</h4>
 									<img src="assets/images/awesome-app/homework-updates.png" />
 								</div>
 								<div class=tab-pane id=parent-app-list-tabs-8 role=tabpanel>
 									<h4>Fee & Online Payment</h4>
 									<img src="assets/images/awesome-app/free-online-payment.png" />
 								</div>
 								<div class=tab-pane id=parent-app-list-tabs-9 role=tabpanel>
 									<h4>Events/Holidays updates </h4>
 									<img src="assets/images/awesome-app/event-holidays-update.png" />
 								</div>
 								<div class=tab-pane id=parent-app-list-tabs-10 role=tabpanel>
 									<h4>School gallery</h4>
 									<img src="assets/images/awesome-app/school-gallary.png" />
 								</div>
								
								 <!-- Download Links New -->
                                     <div class="appDownloadLinks">
                                        <a href="download_apps.php" title="Download Apps" target="_blank"><strong>Download App</strong></a> 
                                     </div>
								
 							</div>
 						</div>
 					</div>
 				</div>
 				<div class=tab-pane id=awesome-app-tabs-2 role=tabpanel>
 					<div class=awesome-app-content-outer>
 						<div class=awesome-app-list-tabs-col>
 							<ul class="nav nav-tabs awesome-app-list-tabs" role=tablist>
 								<li class=nav-item>
 									<a class="nav-link active" data-toggle=tab href="#teacher-app-list-tabs-1" role=tab>
 										<div class=awesome-app-list-nav-link>
 											<span class=grow-tab-icon>
 												<i class="icon-parent_app_icon06"></i> </span>
 											<p> Mark Attendance </p>
 										</div>
 									</a>
 								</li>
 								<li class=nav-item>
 									<a class=nav-link data-toggle=tab href="#teacher-app-list-tabs-2" role=tab>
 										<div class=awesome-app-list-nav-link>
 											<span class=grow-tab-icon>
 												<i class="icon-parent_app_icon11"></i>
 											</span>
 											<p> Share pictures and videos </p>
 										</div>
 									</a>
 								</li>
 								<li class=nav-item>
 									<a class=nav-link data-toggle=tab href="#teacher-app-list-tabs-3" role=tab>
 										<div class=awesome-app-list-nav-link>
 											<span class=grow-tab-icon>
 												<i class="icon-parent_app_icon03"></i>
 											</span>
 											<p> Assign homework </p>
 										</div>
 									</a>
 								</li>
 								<li class=nav-item>
 									<a class=nav-link data-toggle=tab href="#teacher-app-list-tabs-4" role=tab>
 										<div class=awesome-app-list-nav-link>
 											<span class=grow-tab-icon>
 												<i class="icon-parent_app_icon04"></i>
 											</span>
 											<p> Manage leave </p>
 										</div>
 									</a>
 								</li>
 								<li class=nav-item>
 									<a class=nav-link data-toggle=tab href="#teacher-app-list-tabs-5" role=tab>
 										<div class=awesome-app-list-nav-link>
 											<span class=grow-tab-icon>
 												<i class="icon-parent_app_icon05"></i>
 											</span>
 											<p> Schedule exam </p>
 										</div>
 									</a>
 								</li>
 								<li class=nav-item>
 									<a class=nav-link data-toggle=tab href="#teacher-app-list-tabs-6" role=tab>
 										<div class=awesome-app-list-nav-link>
 											<span class=grow-tab-icon>
 												<i class="icon-parent_app_icon02"></i>
 											</span>
 											<p> Notify students </p>
 										</div>
 									</a>
 								</li>
 								<li class=nav-item>
 									<a class=nav-link data-toggle=tab href="#teacher-app-list-tabs-7" role=tab>
 										<div class=awesome-app-list-nav-link>
 											<span class=grow-tab-icon>
 												<i class="icon-parent_app_icon12"></i>
 											</span>
 											<p> Share exam result </p>
 										</div>
 									</a>
 								</li>
 							</ul>
 						</div>
 						<div class=awesome-app-list-content-col>
 							<div class="tab-content awesome-app-list-content">
 								<div class="tab-pane active" id=teacher-app-list-tabs-1 role=tabpanel>
 									<h4>Mark Attendance</h4>
 									<img src="assets/images/awesome-app/mark-attendence.png" />
 								</div>
 								<div class=tab-pane id=teacher-app-list-tabs-2 role=tabpanel>
 									<h4>Share pictures and videos</h4>
 									<img src="assets/images/awesome-app/share-pictures-and-videos.png" />
 								</div>
 								<div class=tab-pane id=teacher-app-list-tabs-3 role=tabpanel>
 									<h4>Assign homework</h4>
 									<img src="assets/images/awesome-app/assign-homework.png" />
 								</div>
 								<div class=tab-pane id=teacher-app-list-tabs-4 role=tabpanel>
 									<h4>Manage leave</h4>
 									<img src="assets/images/awesome-app/manage-leave.png" />
 								</div>
 								<div class=tab-pane id=teacher-app-list-tabs-5 role=tabpanel>
 									<h4>Schedule exam</h4>
 									<img src="assets/images/awesome-app/schedule-exam.png" />
 								</div>
 								<div class=tab-pane id=teacher-app-list-tabs-6 role=tabpanel>
 									<h4>Notify students</h4>
 									<img src="assets/images/awesome-app/notify-students.png" />
 								</div>
 								<div class=tab-pane id=teacher-app-list-tabs-7 role=tabpanel>
 									<h4>Share exam result</h4>
 									<img src="assets/images/awesome-app/share-exam-results.png" />
 								</div>
								
								     <!-- App Download Links -->
                                     <div class="appDownloadLinks">
                                         <a href="https://itunes.apple.com/us/app/schoollog-teacher/id1429753365" target="_blank">
                                            <img src="assets/images/awesome-app/appstore.png"  title="Download App On AppStore">
                                        </a>
                                         <a href="https://play.google.com/store/apps/details?id=in.schoollog.android.teacherapp&hl=en" target="_blank">
                                            <img src="assets/images/awesome-app/playstore.png" title="Download App On PlayStore">
                                        </a>
                                     </div>
								
 							</div>
 						</div>
 					</div>
 				</div>
 				<div class=tab-pane id=awesome-app-tabs-3 role=tabpanel>
 					<div class=awesome-app-content-outer>
 						<div class=awesome-app-list-tabs-col>
 							<ul class="nav nav-tabs awesome-app-list-tabs" role=tablist>
 								<li class=nav-item>
 									<a class="nav-link active" data-toggle=tab href="#director-app-list-tabs-1" role=tab>
 										<div class=awesome-app-list-nav-link>
 											<span class=grow-tab-icon>
 												<i class="icon-parent_app_icon06"></i>
 											</span>
 											<p> Student Attendance </p>
 										</div>
 									</a>
 								</li>
 								<li class=nav-item>
 									<a class=nav-link data-toggle=tab href="#director-app-list-tabs-2" role=tab>
 										<div class=awesome-app-list-nav-link>
 											<span class=grow-tab-icon>
 												<i class="icon-parent_app_icon06"></i>
 											</span>
 											<p> Expense Reports </p>
 										</div>
 									</a>
 								</li>
 								<li class=nav-item>
 									<a class=nav-link data-toggle=tab href="#director-app-list-tabs-3" role=tab>
 										<div class=awesome-app-list-nav-link>
 											<span class=grow-tab-icon>
 												<i class="icon-parent_app_icon03"></i>
 											</span>
 											<p> Teacher Attendance </p>
 										</div>
 									</a>
 								</li>
 								<li class=nav-item>
 									<a class=nav-link data-toggle=tab href="#director-app-list-tabs-4" role=tab>
 										<div class=awesome-app-list-nav-link>
 											<span class=grow-tab-icon>
 												<i class="icon-parent_app_icon04"></i>
 											</span>
 											<p> Income Reports </p>
 										</div>
 									</a>
 								</li>
<!--
 								<li class=nav-item>
 									<a class=nav-link data-toggle=tab href="#director-app-list-tabs-5" role=tab>
 										<div class=awesome-app-list-nav-link>
 											<span class=grow-tab-icon>
 												<i class="icon-parent_app_icon05"></i>
 											</span>
 											<p> Teacher Performance </p>
 										</div>
 									</a>
 								</li>
-->
 								<li class=nav-item>
 									<a class=nav-link data-toggle=tab href="#director-app-list-tabs-6" role=tab>
 										<div class=awesome-app-list-nav-link>
 											<span class=grow-tab-icon>
 												<i class="icon-parent_app_icon06"></i>
 											</span>
 											<p> Manage Staff Leave </p>
 										</div>
 									</a>
 								</li>
 								<li class=nav-item>
 									<a class=nav-link data-toggle=tab href="#director-app-list-tabs-7" role=tab>
 										<div class=awesome-app-list-nav-link>
 											<span class=grow-tab-icon>
 												<i class="icon-parent_app_icon07"></i>
 											</span>
 											<p> Daily Collection </p>
 										</div>
 									</a>
 								</li>
 								<li class=nav-item>
 									<a class=nav-link data-toggle=tab href="#director-app-list-tabs-8" role=tab>
 										<div class=awesome-app-list-nav-link>
 											<span class=grow-tab-icon>
 												<i class="icon-parent_app_icon02"></i>
 											</span>
 											<p> Send Notifications </p>
 										</div>
 									</a>
 								</li>
 								<li class=nav-item>
 									<a class=nav-link data-toggle=tab href="#director-app-list-tabs-9" role=tab>
 										<div class=awesome-app-list-nav-link>
 											<span class=grow-tab-icon>
 												<i class="icon-parent_app_icon06"></i>
 											</span>
 											<p> Concession Reports </p>
 										</div>
 									</a>
 								</li>
 								<li class=nav-item>
 									<a class=nav-link data-toggle=tab href="#director-app-list-tabs-10" role=tab>
 										<div class=awesome-app-list-nav-link>
 											<span class=grow-tab-icon>
 												<i class="icon-parent_app_icon10"></i>
 											</span>
 											<p> Manage School Gallery </p>
 										</div>
 									</a>
 								</li>
 							</ul>
 						</div>
 						<div class=awesome-app-list-content-col>
 							<div class="tab-content awesome-app-list-content">
 								<div class="tab-pane active" id=director-app-list-tabs-1 role=tabpanel>
 									<h4>Student Attendance</h4>
 									<img src="assets/images/awesome-app/student-attendence.png" />
 								</div>
 								<div class=tab-pane id=director-app-list-tabs-2 role=tabpanel>
 									<h4>Expense Reports</h4>
 									<img src="assets/images/awesome-app/expance-reports.png" />
 								</div>
 								<div class=tab-pane id=director-app-list-tabs-3 role=tabpanel>
 									<h4>Teacher Attendance</h4>
 									<img src="assets/images/awesome-app/teacher-attendence.png" />
 								</div>
 								<div class=tab-pane id=director-app-list-tabs-4 role=tabpanel>
 									<h4>Income Reports</h4>
 									<img src="assets/images/awesome-app/income-reports.png" />
 								</div>
<!--
 								<div class=tab-pane id=director-app-list-tabs-5 role=tabpanel>
 									<h4>Teacher Performance</h4>
 									<img src="assets/images/director_app02.jpg" />
 								</div>
-->
 								<div class=tab-pane id=director-app-list-tabs-6 role=tabpanel>
 									<h4>Manage Staff Leave</h4>
 									<img src="assets/images/awesome-app/staff-leave.png" />
 								</div>
 								<div class=tab-pane id=director-app-list-tabs-7 role=tabpanel>
 									<h4>Daily Collection</h4>
 									<img src="assets/images/awesome-app/daily-collections.png" />
 								</div>
 								<div class=tab-pane id=director-app-list-tabs-8 role=tabpanel>
 									<h4>Send Notifications</h4>
 									<img src="assets/images/awesome-app/send-notifications.png" />
 								</div>
 								<div class=tab-pane id=director-app-list-tabs-9 role=tabpanel>
 									<h4>Concession Reports</h4>
 									<img src="assets/images/awesome-app/income-reports.png" />
 								</div>
 								<div class=tab-pane id=director-app-list-tabs-10 role=tabpanel>
 									<h4>Manage School Gallery</h4>
 									<img src="assets/images/awesome-app/manage-school-gallary.png" />
 								</div>
								
								 <!-- App Download Links -->
                                     <div class="appDownloadLinks">
                                         <a href="https://itunes.apple.com/us/app/schoollog-director/id1438827202" target="_blank">
                                            <img src="assets/images/awesome-app/appstore.png"  title="Download App On AppStore">
                                        </a>
                                         <a href="https://play.google.com/store/apps/details?id=in.schoollog.android.directorapp" target="_blank">
                                            <img src="assets/images/awesome-app/playstore.png" title="Download App On PlayStore">
                                        </a>
                                     </div>
								
 							</div>
 						</div>
 					</div>
 				</div>
 				<div class=tab-pane id=awesome-app-tabs-4 role=tabpanel>
 					<div class=awesome-app-content-outer>
 						<div class=awesome-app-list-tabs-col>
 							<ul class="nav nav-tabs awesome-app-list-tabs" role=tablist>
 								<li class=nav-item>
 									<a class="nav-link active" data-toggle=tab href="#principal-app-list-tabs-1" role=tab>
 										<div class=awesome-app-list-nav-link>
 											<span class=grow-tab-icon>
 												<i class="icon-parent_app_icon06"></i>
 											</span>
 											<p> Student Attendance</p>
 										</div>
 									</a>
 								</li>
 								<li class=nav-item>
 									<a class=nav-link data-toggle=tab href="#principal-app-list-tabs-2" role=tab>
 										<div class=awesome-app-list-nav-link>
 											<span class=grow-tab-icon>
 												<i class="icon-parent_app_icon05"></i>
 											</span>
 											<p> Assign homework </p>
 										</div>
 									</a>
 								</li>
 								<li class=nav-item>
 									<a class=nav-link data-toggle=tab href="#principal-app-list-tabs-3" role=tab>
 										<div class=awesome-app-list-nav-link>
 											<span class=grow-tab-icon>
 												<i class="icon-parent_app_icon03"></i>
 											</span>
 											<p> Teacher Attendance </p>
 										</div>
 									</a>
 								</li>
 								<li class=nav-item>
 									<a class=nav-link data-toggle=tab href="#principal-app-list-tabs-4" role=tab>
 										<div class=awesome-app-list-nav-link>
 											<span class=grow-tab-icon>
 												<i class="icon-parent_app_icon04"></i>
 											</span>
 											<p> Schedule exam </p>
 										</div>
 									</a>
 								</li>
 								<li class=nav-item>
 									<a class=nav-link data-toggle=tab href="#principal-app-list-tabs-5" role=tab>
 										<div class=awesome-app-list-nav-link>
 											<span class=grow-tab-icon>
 												<i class="icon-parent_app_icon02"></i>
 											</span>
 											<p> Send Notifications </p>
 										</div>
 									</a>
 								</li>
 								<li class=nav-item>
 									<a class=nav-link data-toggle=tab href="#principal-app-list-tabs-6" role=tab>
 										<div class=awesome-app-list-nav-link>
 											<span class=grow-tab-icon>
 												<i class="icon-parent_app_icon12"></i>
 											</span>
 											<p> Share exam result </p>
 										</div>
 									</a>
 								</li>
 								<li class=nav-item>
 									<a class=nav-link data-toggle=tab href="#principal-app-list-tabs-7" role=tab>
 										<div class=awesome-app-list-nav-link>
 											<span class=grow-tab-icon>
 												<i class="icon-parent_app_icon11"></i>
 											</span>
 											<p> Manage School Gallery </p>
 										</div>
 									</a>
 								</li>
 							</ul>
 						</div>
 						<div class=awesome-app-list-content-col>
 							<div class="tab-content awesome-app-list-content">
 								<div class="tab-pane active" id=principal-app-list-tabs-1 role=tabpanel>
 									<h4>Student Attendance</h4>
 									<img src="assets/images/awesome-app/student-attendence.png" />
 								</div>
 								<div class=tab-pane id=principal-app-list-tabs-2 role=tabpanel>
 									<h4>Assign homework</h4>
 									<img src="assets/images/awesome-app/assign-homework.png" />
 								</div>
 								<div class=tab-pane id=principal-app-list-tabs-3 role=tabpanel>
 									<h4>Teacher Attendance</h4>
 									<img src="assets/images/awesome-app/teacher-attendence.png" />
 								</div>
 								<div class=tab-pane id=principal-app-list-tabs-4 role=tabpanel>
 									<h4>Schedule exam</h4>
 									<img src="assets/images/awesome-app/schedule-exam.png" />
 								</div>
 								<div class=tab-pane id=principal-app-list-tabs-5 role=tabpanel>
 									<h4>Send Notifications</h4>
 									<img src="assets/images/awesome-app/send-notifications.png" />
 								</div>
 								<div class=tab-pane id=principal-app-list-tabs-6 role=tabpanel>
 									<h4>Share exam result</h4>
 									<img src="assets/images/awesome-app/share-exam-results.png" />
 								</div>
 								<div class=tab-pane id=principal-app-list-tabs-7 role=tabpanel>
 									<h4>Manage School Gallery</h4>
 									<img src="assets/images/awesome-app/manage-school-gallary.png" />
 								</div>
								 <!-- App Download Links -->
                                     <div class="appDownloadLinks">
                                         <a href="https://itunes.apple.com/us/app/schoollog-director/id1438827202" target="_blank">
                                            <img src="assets/images/awesome-app/appstore.png"  title="Download App On AppStore">
                                        </a>
                                         <a href="https://play.google.com/store/apps/details?id=in.schoollog.android.directorapp" target="_blank">
                                            <img src="assets/images/awesome-app/playstore.png" title="Download App On PlayStore">
                                        </a>
                                     </div>
								
								
 							</div>
 						</div>
 					</div>
 				</div>
 				<div class=tab-pane id=awesome-app-tabs-5 role=tabpanel>
 					<div class=awesome-app-content-outer>
 						<div class=awesome-app-list-tabs-col>
 							<ul class="nav nav-tabs awesome-app-list-tabs" role=tablist>
 								<li class=nav-item>
 									<a class="nav-link active" data-toggle=tab href="#admin-app-list-tabs-1" role=tab>
 										<div class=awesome-app-list-nav-link>
 											<span class=grow-tab-icon>
 												<i class="icon-parent_app_icon06"></i>
 											</span>
 											<p>  Attendance Management </p>
 										</div>
 									</a>
 								</li>
 								<li class=nav-item>
 									<a class=nav-link data-toggle=tab href="#admin-app-list-tabs-2" role=tab>
 										<div class=awesome-app-list-nav-link>
 											<span class=grow-tab-icon>
 												<i class="icon-parent_app_icon16"></i>
 											</span>
 											<p> Expense Reports </p>
 										</div>
 									</a>
 								</li>
 								<li class=nav-item>
 									<a class=nav-link data-toggle=tab href="#admin-app-list-tabs-3" role=tab>
 										<div class=awesome-app-list-nav-link>
 											<span class=grow-tab-icon>
 												<i class="icon-parent_app_icon03"></i>
 											</span>
 											<p>  Staff Management </p>
 										</div>
 									</a>
 								</li>
 								<li class=nav-item>
 									<a class=nav-link data-toggle=tab href="#admin-app-list-tabs-4" role=tab>
 										<div class=awesome-app-list-nav-link>
 											<span class=grow-tab-icon>
 												<i class="icon-parent_app_icon08"></i>
 											</span>
 											<p>  Income Reports </p>
 										</div>
 									</a>
 								</li>
 								<li class=nav-item>
 									<a class=nav-link data-toggle=tab href="#admin-app-list-tabs-5" role=tab>
 										<div class=awesome-app-list-nav-link>
 											<span class=grow-tab-icon>
 												<i class="icon-parent_app_icon15"></i>
 											</span>
 											<p>  Staff Leave Management </p>
 										</div>
 									</a>
 								</li>
 								<li class=nav-item>
 									<a class=nav-link data-toggle=tab href="#admin-app-list-tabs-6" role=tab>
 										<div class=awesome-app-list-nav-link>
 											<span class=grow-tab-icon>
 												<i class="icon-parent_app_icon06"></i>
 											</span>
 											<p> Collection Report </p>
 										</div>
 									</a>
 								</li>
 								<li class=nav-item>
 									<a class=nav-link data-toggle=tab href="#admin-app-list-tabs-7" role=tab>
 										<div class=awesome-app-list-nav-link>
 											<span class=grow-tab-icon>
 												<i class="icon-parent_app_icon14"></i>
 											</span>
 											<p> Send Notifications </p>
 										</div>
 									</a>
 								</li>
 								<li class=nav-item>
 									<a class=nav-link data-toggle=tab href="#admin-app-list-tabs-8" role=tab>
 										<div class=awesome-app-list-nav-link>
 											<span class=grow-tab-icon>
 												<i class="icon-parent_app_icon13"></i>
 											</span>
 											<p>  Manage Concession </p>
 										</div>
 									</a>
 								</li>
								
								<li class=nav-item>
 									<a class=nav-link data-toggle=tab href="#admin-app-list-tabs-9" role=tab>
 										<div class=awesome-app-list-nav-link>
 											<span class=grow-tab-icon>
 												<i class="icon-parent_app_icon10"></i>
 											</span>
 											<p>  Manage School Gallery  </p>
 										</div>
 									</a>
 								</li>
								
 							</ul>
 						</div>
 						<div class=awesome-app-list-content-col>
 							<div class="tab-content awesome-app-list-content">
 								<div class="tab-pane active" id=admin-app-list-tabs-1 role=tabpanel>
 									<h4>Attendance Management</h4>
 									<img src="assets/images/awesome-app/student-attendence.png" />
 								</div>
 								<div class=tab-pane id=admin-app-list-tabs-2 role=tabpanel>
 									<h4>Expense Reports</h4>
 									<img src="assets/images/awesome-app/expance-reports.png" />
 								</div>
 								<div class=tab-pane id=admin-app-list-tabs-3 role=tabpanel>
 									<h4>Teacher Attendance</h4>
 									<img src="assets/images/awesome-app/teacher-attendence.png" />
 								</div>
 								<div class=tab-pane id=admin-app-list-tabs-4 role=tabpanel>
 									<h4>Income Reports</h4>
 									<img src="assets/images/awesome-app/income-reports.png" />
 								</div>
 								<div class=tab-pane id=admin-app-list-tabs-5 role=tabpanel>
 									<h4>Manage Staff Leave</h4>
 									<img src="assets/images/awesome-app/staff-leave.png" />
 								</div>
 								<div class=tab-pane id=admin-app-list-tabs-6 role=tabpanel>
 									<h4>Daily Collection</h4>
 									<img src="assets/images/awesome-app/daily-collections.png" />
 								</div>
 								<div class=tab-pane id=admin-app-list-tabs-7 role=tabpanel>
 									<h4>Send Notifications</h4>
 									<img src="assets/images/awesome-app/send-notifications.png" />
 								</div>
 								<div class=tab-pane id=admin-app-list-tabs-8 role=tabpanel>
 									<h4>Concession Reports</h4>
 									<img src="assets/images/awesome-app/income-reports.png" />
 								</div>
								
								<div class=tab-pane id=admin-app-list-tabs-8 role=tabpanel>
 									<h4>Manage School Gallery</h4>
 									<img src="assets/images/awesome-app/manage-school-gallary.png" />
 								</div>
								
								<!-- App Download Links -->
                                     <div class="appDownloadLinks">
                                         <!-- <a href="https://itunes.apple.com/in/app/schoollog-parent-app/id1168033671?mt=8" target="_blank">
                                            <img style="max-height:100%;max-width:100%" src="assets/images/awesome-app/appstore.png"  title="Download App On AppStore">
                                        </a> -->
                                         <a href="https://play.google.com/store/apps/details?id=in.schoollog.android.adminapp&hl=en" target="_blank">
                                            <img src="assets/images/awesome-app/playstore.png" title="Download App On PlayStore">
                                        </a>
                                     </div>
								
								
 							</div>
 						</div>
 					</div>
 				</div>
 			</div>

 			<div class=panel-group id=accordion role=tablist aria-multiselectable=true></div>
 		</div>
 	</section>
 	<!-- //.Section 03 -->


 	<!-- Section 04 -->
 	<section class="schoollog-loved-section wow fadeInUp" id="section05">
 		<a class="scroll-next" href="#section06"><span></span><span></span><span></span>Scroll</a>
 		<div class=container>
 			<p class="wow fadeInLeft">
 				Loved by <span>100000</span> students, teachers & parents <br>
 				from India’s best schools. Schools trust Schoollog!
 				<p>
					
 
					
			 
					<ul class="slides happy-clients">
						<div class="client-logo-carousel">
							<li>
								<a class="" href="#"> 
									<img src="assets/images/clients/client_logo_0.png">  
								</a>
							</li>

							<li>
								<a class="" href="#"> 
									<img src="assets/images/clients/client_logo_1.png">  
								</a>
							</li>

							<li>
								<a class="" href="#">
									<img src="assets/images/clients/client_logo_2.png">  
								</a>
							</li> 
							<li>
								<a class="" href="#">
									<img src="assets/images/clients/client_logo_3.png">  
								</a>
							</li> 
							
							<li>
								<a class="" href="#">
									<img src="assets/images/clients/client_logo_4.png">  
								</a>
							</li> 
							
							<li>
								<a class="" href="#">
									<img src="assets/images/clients/client_logo_5.png">  
								</a>
							</li> 
							
							<li>
								<a class="" href="#">
									<img src="assets/images/clients/client_logo_6.png">  
								</a>
							</li> 
							
							<li>
								<a class="" href="#"> 
									<img src="assets/images/clients/client_logo_7.png">  
								</a>
							</li> 
							
							<li>
								<a class="" href="#">
									<img src="assets/images/clients/client_logo_8.png">  
								</a>
							</li> 
							
							
						</div>
					</ul>
				 
		 
		 

 		</div>
 	</section>
 	<!--// Section 04 -->

 	<!-- Section 05 -->
 	<section class="schoollog-area wow fadeInUp" id="section06">
 		<a class="scroll-next" href="#section07"><span></span><span></span><span></span>Scroll</a>
 		<div class=container>
 			<div class=row>
 				<div class="col-md-6 wow fadeInLeft">
 					<sapn class=country-map><img src="assets/images/schoollog_india.png" /></sapn>
 				</div>
 				<div class="col-md-6 wow fadeInRight">
 					<div class="info-block info-block--visual">
 						<div class=infoRotator>
 							<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAABBJREFUeNpi+P//PwNAgAEACPwC/tuiTRYAAAAASUVORK5CYII=" class=infoRotator-placeholder data-pagespeed-url-hash=606200798>
 							<div class=infoRotator-wrapper>
 								<div class="infoRotator-circle infoRotator-circle--1">
 									<div class=infoRotator-marker></div>
 									<div class=infoRotator-marker></div>
 									<div class=infoRotator-marker></div>
 									<div class=infoRotator-marker></div>
 									<div class=infoRotator-marker></div>
 									<div class=infoRotator-marker></div>
 									<div class=infoRotator-marker></div>
 									<div class=infoRotator-marker></div>
 								</div>
 								<div class="infoRotator-circle infoRotator-circle--2">
 									<div class=infoRotator-marker><a class=infoRotator-link href="#" data-num=1></a></div>
 									<div class=infoRotator-marker><a class=infoRotator-link href="#" data-num=2></a></div>
 									<div class=infoRotator-marker><a class=infoRotator-link href="#" data-num=3></a></div>
 									<div class=infoRotator-marker><a class=infoRotator-link href="#" data-num=4></a></div>
 									<div class=infoRotator-marker><a class=infoRotator-link href="#" data-num=5></a></div>
 									<div class=infoRotator-marker><a class="infoRotator-link active" href="#" data-num=6></a></div>
 									<div class=infoRotator-marker><a class=infoRotator-link href="#" data-num=7></a></div>
 									<div class=infoRotator-marker><a class=infoRotator-link href="#" data-num=8></a></div>
 									<div class=infoRotator-marker><a class=infoRotator-link href="#" data-num=9></a></div>
 									<div class=infoRotator-content>
 										<div class="infoRotator-eclipse animation" style="display: block;"></div>
 										<div class=infoRotator-box data-num=1 style="display: none;">
 											<table class=infoRotator-table>
 												<tbody>
 													<tr>
 														<td>
 															<h4>4.8Lakh</h4>
 															<p>Student</p>
 														</td>
 													</tr>
 												</tbody>
 											</table>
 										</div>
 										<div class=infoRotator-box data-num=2 style="display: none;">
 											<table class=infoRotator-table>
 												<tbody>
 													<tr>
 														<td>
 															<h4>2000</h4>
 															<p>Tearcher</p>
 														</td>
 													</tr>
 												</tbody>
 											</table>
 										</div>
 										<div class=infoRotator-box data-num=3 style="display: none;">
 											<table class=infoRotator-table>
 												<tbody>
 													<tr>
 														<td>
 															<h4>200</h4>
 															<p>Rooms</p>
 														</td>
 													</tr>
 												</tbody>
 											</table>
 										</div>
 										<div class=infoRotator-box data-num=4 style="display: none;">
 											<table class=infoRotator-table>
 												<tbody>
 													<tr>
 														<td>
 															<h4>100+</h4>
 															<p>Courses</p>
 														</td>
 													</tr>
 												</tbody>
 											</table>
 										</div>
 										<div class=infoRotator-box data-num=5 style="display: none;">
 											<table class=infoRotator-table>
 												<tbody>
 													<tr>
 														<td>
 															<h4>50+</h4>
 															<p>Faculties</p>
 														</td>
 													</tr>
 												</tbody>
 											</table>
 										</div>
 										<div class="infoRotator-box active" data-num=6 style="display: block;">
 											<table class=infoRotator-table>
 												<tbody>
 													<tr>
 														<td>
 															<h4>300</h4>
 															<p>Examinar</p>
 														</td>
 													</tr>
 												</tbody>
 											</table>
 										</div>
 										<div class=infoRotator-box data-num=7 style="display: none;">
 											<table class=infoRotator-table>
 												<tbody>
 													<tr>
 														<td>
 															<h4>5.1Lakhs</h4>
 															<p>Student</p>
 														</td>
 													</tr>
 												</tbody>
 											</table>
 										</div>
 										<div class=infoRotator-box data-num=8 style="display: none;">
 											<table class=infoRotator-table>
 												<tbody>
 													<tr>
 														<td>
 															<h4>5+</h4>
 															<p>Sports Grounds</p>
 														</td>
 													</tr>
 												</tbody>
 											</table>
 										</div>
 										<div class=infoRotator-box data-num=9 style="display: none;">
 											<table class=infoRotator-table>
 												<tbody>
 													<tr>
 														<td>
 															<h4>100%</h4>
 															<p>Attendance</p>
 														</td>
 													</tr>
 												</tbody>
 											</table>
 										</div>
 									</div>
 								</div>
 								<div class="infoRotator-circle infoRotator-circle--3">
 									<div class=infoRotator-marker></div>
 									<div class=infoRotator-marker></div>
 									<div class=infoRotator-marker></div>
 									<div class=infoRotator-marker></div>
 									<div class=infoRotator-marker></div>
 									<div class=infoRotator-marker></div>
 									<div class=infoRotator-marker></div>
 									<div class=infoRotator-marker></div>
 								</div>
 							</div>
 						</div>
 					</div>
 				</div>

 			</div>
 		</div>
 	</section>
 	<!--// Section 05 -->
 

<!-- free-trial-section -->
<section class="free-trial-section wow fadeInUp" id="section07">
	 <?php 
		include 'freeTrialSection.php';
		freeTrialSection("Choose Us","How to save time, reduce your workload
and enhance learning?");
		?>
</section> 
<!--// free-trial-section -->



 <?php include 'footer.php';?> 
<script src="assets/js/infoRotator-circle.js.download"></script>
<script type="text/javascript" src="assets/js/owl.carousel.min.js"></script>
