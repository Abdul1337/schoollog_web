<?php include 'header.php';?>  

 <!-- Banner Section -->

    <div class="inner-page-header">
        <?php include 'header.php';?>
        <div class="clearfix"></div>
        <div class="full">
            <div class="inner-show-img"><img src="assets/images/faq.jpg"></div>
            <div class="container">
                <div class="slider-content">
                    <h1>Faq</h1>
                    <p class="subheading">STILL HAVE DOUBTS? We’ll Clear Them all!</p>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>


    <!-- //.Banner Banner -->

    <!--Banner-->

    <section class="counication-page our-team our-reason">

        <div class="container">
            <div class="reason-list">
                <div class="awesome-app-content-outer faq-page">
                    <div class="awesome-app-list-tabs-col">
                        <ul class="nav nav-tabs awesome-app-list-tabs" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#parent-app-list-tabs-1" role="tab">
                                    <div class="awesome-app-list-nav-link">
                                        <p>
                                            Q1
                                        </p>
                                    </div>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#parent-app-list-tabs-2" role="tab">
                                    <div class="awesome-app-list-nav-link">
                                        <p>
                                            Q2
                                        </p>
                                    </div>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#parent-app-list-tabs-3" role="tab">
                                    <div class="awesome-app-list-nav-link">
                                        <p>
                                            Q3
                                        </p>
                                    </div>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#parent-app-list-tabs-4" role="tab">
                                    <div class="awesome-app-list-nav-link">
                                        <p>
                                            Q4
                                        </p>
                                    </div>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#parent-app-list-tabs-5" role="tab">
                                    <div class="awesome-app-list-nav-link">
                                        <p>
                                            Q5
                                        </p>
                                    </div>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#parent-app-list-tabs-6" role="tab">
                                    <div class="awesome-app-list-nav-link">
                                        <p>
                                            Q6
                                        </p>
                                    </div>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#parent-app-list-tabs-7" role="tab">
                                    <div class="awesome-app-list-nav-link">
                                        <p>
                                            Q7
                                        </p>
                                    </div>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#parent-app-list-tabs-8" role="tab">
                                    <div class="awesome-app-list-nav-link">
                                        <p>
                                            Q8
                                        </p>
                                    </div>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#parent-app-list-tabs-9" role="tab">
                                    <div class="awesome-app-list-nav-link">
                                        <p>
                                            Q9
                                        </p>
                                    </div>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#parent-app-list-tabs-10" role="tab">
                                    <div class="awesome-app-list-nav-link">
                                        <p>
                                            Q10
                                        </p>
                                    </div>
                                </a>
                            </li>

                        </ul>
                    </div>
                    <div class="awesome-app-list-content-col">
                        <div class="tab-content awesome-app-list-content wow fadeInRight" style="margin-top:60px">
                            <div class="tab-pane active" id="parent-app-list-tabs-1" role="tabpanel">
                                <h5>What Is Schoollog?</h5>
                                <p>Schoollog is a web and app-based management solution for schools which put a school
                                    on autopilot mode and focuses on Parent-Teacher communication through its app,
                                    resulting in enhanced performance of the student as well as school.</p>
                            </div>
                            <div class="tab-pane" id="parent-app-list-tabs-2" role="tabpanel">
                                <h5>How can I manage my school using Schoollog? </h5>
                                <p>All the school management chores can be done using 50+ different portals of
                                    Schoollog, such as Admin panel for administrative tasks, Accounts panel for handling
                                    fee transactions, Exam panel for exam management, reception panel for managing
                                    admissions, etc. </p>
                            </div>
                            <div class="tab-pane" id="parent-app-list-tabs-3" role="tabpanel">
                                <h5>What does our school need to prepare when we implement Schoollog and how much time
                                    it takes? </h5>
                                <p>All you need to do is provide us your staff and student data in excel and our team
                                    get your account ready in less than a day. </p>
                            </div>
                            <div class="tab-pane" id="parent-app-list-tabs-4" role="tabpanel">
                                <h5>Do You Provide Training? </h5>
                                <p>We certainly do. All new customers receive training as part of their initial setup.
                                    This can be fulfilled on campus or remotely. In addition, custom training is
                                    available as needed. Finally, periodic online training sessions are provided at no
                                    additional cost to our customers.</p>
                            </div>
                            <div class="tab-pane" id="parent-app-list-tabs-5" role="tabpanel">
                                <h5>How much will Schoollog cost me? </h5>
                                <p>We have plans that fit all kind of schools, to know the best plan for your school
                                    gets in touch with our sales executive at <a
                                        href="tel:7726005349">+91-7726005349</a>.</p>
                            </div>
                            <div class="tab-pane" id="parent-app-list-tabs-6" role="tabpanel">
                                <h5>How can I get my Schoollog app username/password? </h5>
                                <p>Initially, the username password is sent by the school on registered mobile number.
                                    If you have lost that, then <br> Parents can contact the school for new username
                                    password. <br>School staff can contact school admin for new username password.</p>
                            </div>
                            <div class="tab-pane" id="parent-app-list-tabs-7" role="tabpanel">
                                <h5>Can I import my existing data into Schoollog?</h5>
                                <p>Well, you can. There will be no problem at all.</p>
                            </div>
                            <div class="tab-pane" id="parent-app-list-tabs-8" role="tabpanel">
                                <h5>Do I need to pay extra if new features are added to my schoollog?</h5>
                                <p>No. We will not charge our existing customers for solution upgrade</p>
                            </div>
                            <div class="tab-pane" id="parent-app-list-tabs-9" role="tabpanel">
                                <h5>Is the Student Information kept over the years?</h5>
                                <p>Yes, and it is easily accessible. You can see the history of a student from the
                                    moment they join till they leave.</p>
                            </div>
                            <div class="tab-pane" id="parent-app-list-tabs-10" role="tabpanel">
                                <h5>We Still Have Many Unanswered Questions, What We Should Do?</h5>
                                <p>Get in touch with us at <br><a href="tel:8448443326">+91-8448443326</a> or <a
                                        href="mailto:info@schoollog.in">info@schoollog.in</a>, we will answer all your
                                    queries.</p>
                            </div>


                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <!--//.Banner-->



<!-- free-trial-section -->
<section class="free-trial-section wow fadeInUp">
	 <?php 
		include 'freeTrialSection.php';
		freeTrialSection("Want to See","How to save time, reduce your workload<br/>and enhance learning?");
		?>
</section> 
<!--// free-trial-section --> 
<?php include 'footer.php';?>


 