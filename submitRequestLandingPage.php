<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\SMTP;
require 'vendor\autoload.php';

$string = file_get_contents("config.json");
$option = json_decode($string);

define("MAIL_HOST", $option->MAIL_HOST);
define("MAIL_HOSTNAME", $option->MAIL_HOSTNAME);
define("MAIL_USERNAME", $option->MAIL_USERNAME);
define("MAIL_PASSWORD", $option->MAIL_PASSWORD);
define("MAIL_TITLE", $option->MAIL_TITLE_LANDING_PAGE);
define("SUCCESS_MSG", $option->SUCCESS_MSG);
define("FAILURE_MSG", $option->FAILURE_MSG);


if( isset($_POST['name']) && isset($_POST['mobile']) && isset($_POST['email'])) {   

    // Data recieved from Form

    $name = $_POST['name'];
    $mobile = $_POST['mobile'];
    $email = $_POST['email'];
    $msg = $_POST['msg'];


    // Mail Content
    $mail_subject =MAIL_TITLE." from $email";
    $mail_body = "
    Name : $name<br>
    Mobile : $mobile<br>
    E-mail : $email<br>
    Message : $msg<br>
    ";

    // Mail Configuration
    $mail = new PHPMailer();
    $mail->SMTPDebug  = 0;  
    $mail->IsSMTP(true);
    $mail->Host= "smtp.gmail.com";
    $mail->SMTPAuth   = true;
    $mail->Username   = MAIL_USERNAME;
    $mail->Password   = MAIL_PASSWORD;
    $mail->SMTPSecure = "tls";
    $mail->Port       = 25;

    $mail->SetFrom(MAIL_USERNAME, MAIL_HOSTNAME);
    $mail->AddAddress(MAIL_HOST);

    // Mail Content
    $mail->IsHTML(true);
    $mail->Subject = $mail_subject;
    $mail->Body = $mail_body;

    $succcess = false;
    if($mail->send()){
        $success_msg = SUCCESS_MSG;
        $serialized_data = '{"type":"success",  "message":"'."$success_msg".'"}';
        echo $serialized_data;
    }else{
        $fail_msg = FAILURE_MSG;
        $serialized_data = '{"type":"error", "message":"'."$fail_msg".'"}';
        echo $serialized_data;
    }
}else{
    $serialized_data = '{"type":"error",  "message":"Form fields not filled."}';
    echo $serialized_data;
}
?>