<?php include 'header.php';?>  

   <!-- Banner Section --> 
    <div class="inner-page-header">
        <div class="full">
            <div class="inner-show-img"><img src="assets/images/term_conditation.jpg"></div>
            <div class="container">
                <div class="slider-content">
                    <h1>Terms of Use</h1>

                </div>
            </div>
			 <div class="clearfix"></div>

        </div>
        <div class="clearfix"></div>
    </div>


    <!-- //.Banner Section -->

    <!--Section-->

    <section class="counication-page our-team our-reason">

        <div class="container">
            <div class="reason-list">
                <div class="top-text"></div>

                <div class="reason-108 terms">
                    <h4>Service Offered</h4>
                    <p>The services of this website Schoollog are available only to the registered users on single user basis, for a particular period of time on making the stipulated payment and abiding by the applicable terms and conditions. The said services are personal in nature and cannot be assigned or transferred to or shared with any other person other than the registered users.</p>
                    <p>By registering yourself as a user and subscribing to avail the services of this website Schoollog it will be deemed that you have read, understood and agreed to fully abide by all the terms and conditions of the website as contained herein.</p>

                    <p>The right to use the services of the website Schoollog is on revocable license / permission basis as per the terms and conditions contained herein. Except the availment of the services during the license period, the registered users shall not have any right or title over the website or any of its contents.</p>

                    <p>The services offered by Schoollog through this site are listed in detail in the home page of this site.</p>
                    <p>Schoollog currently provides users with access to a rich collection/compilation of online educational information and related resources etc. As a user of this site, you understand and agree that all the services provided are on the basis of as-it-is and Schoollog assume no liability for the accuracy or completeness or use of, nor any liability to update, the information contained on this website.</p>

                    <p>In order to use Schoollog online services, you must obtain access to the World Wide Web, either directly or through devices that access web-based content, and pay any service fees associated with such access. In addition, you must provide all equipment necessary to make such a connection to the World Wide Web, including a computer and modem or other access devices.</p>

                    <h4>User Obligations</h4>
                    <p>In consideration of your use of the services, you agree to: provide correct, precise, upto date and complete information about yourself as required by the service's registration form wherever necessary; and maintain and promptly update the registration data to keep it true, accurate, current and complete.</p>
                    <p>If you provide any information that is untrue, inaccurate, not up to date or incomplete, or Schoollog has reasonable grounds to suspect that such information is untrue, inaccurate, not current or incomplete, Schoollog has the right to suspend or terminate your account and refuse any and all current or future use of the services (or any portion thereof).</p>
                    <!-- <p>If you are directly subscribing as a user of this website, you represent and warrant that you are at least 18 years of age and that you possess the legal right and capacity to use the services of Schoollog in accordance with the stated terms and usage policies. In cases, where a minor below the age of 18 years of age, wants to use this website, such a user shall duly register himself through his mother/father and natural guardian and such a mother/father and natural guardian hereby agree to accordingly register and supervise usage by, and be responsible for the action of any such minors who use the computer and/or password to access the Next Education India (P) Ltd. The mother/father and natural guardian shall enter into this agreement on behalf of such minor, and bind himself/herself in accordance with all terms and conditions herein.</p> -->
                    <h4>Agreement</h4>
                    <p>You agree and are hereby put on notice, that you are prohibited from posting or transmitting to this website, any content/material that in unlawful, threatening, libelous, defamatory, pornographic or that in any manner violate or infringe the proprietary rights, intellectual property rights or any right or privacy of third party whatsoever and that violate any law, statute, ordinance or regulation; and that contain viruses or other similar harmful or threatening deleterious programming routines. If you violate this provision you will be finalised apart from termination of your rights to use the services and any other remedies available to Schoollog</p>
                    <p>Software (if any) that is made available to download from the Schoollog site, excluding software that may be made available by end-users through a Communication Service, (Software) is the copyrighted work of the software provider. Your use of the Software is governed by the terms of the end user License Agreement unless you first agree to the License Agreement terms.</p>
                    <p>Schoollog reserves the right, in its sole discretion, to terminate or refuse all or part of its services, for any reason without notice to you.</p>
                    <p>This Agreement shall be governed by the Laws in India and in all disputes arising out of or relating to the use of Schoollog, you hereby consent to the exclusive jurisdiction and venue of courts in Mumbai, India (only).</p>
                    <p>This Agreement does not in any manner create any relationship whatsoever as between you and the Schoollog , either as a joint venture, partnership, employment, or agency relationship. Performance of this agreement by Schoollog is subject to existing laws and legal process in India, and nothing contained in this agreement is in derogation of the rights of Schoollog to comply with governmental, court and law enforcement requests or requirements relating to your use of Schoollog or information provided to or gathered by Schoollog with respect to such use.</p>
                    <p>Unless otherwise specified herein, this Agreement constitutes the entire agreement between the user and Schoollog with respect to the Schoollog and it supersedes all prior communications and proposals, whether electronic, oral or written, between the user and Schoollog with respect to the Schoollog site.</p>

                    <!-- <h4>Cancellation and Refund Policy</h4>
                    <p>Please note that Packages once subscribed cannot be cancelled and no refund to that effect can be made. Since LearnNext.com is offering non-tangible irrevocable goods we do not issue refunds once the package is subscribed. As a customer you are responsible for understanding that no refund or cancellation is possible upon any subscription at our site.</p>

                    <h4>Delivery policy</h4>
                    <p>Delivery means the digital download or the physical delivery of subscribed content to the customers. At present we have two modes of delivery – online digital download from the website and physical shipment like CDROM, USB memory etc.</p>
                     -->
                </div>
            </div>
        </div>
    </section>
    <!--Section-->



<!-- free-trial-section -->
<section class="free-trial-section wow fadeInUp">
	 <?php 
		include 'freeTrialSection.php';
		freeTrialSection("Want to See","How to save time, reduce your workload<br/>and enhance learning?");
		?>
</section> 
<!--// free-trial-section --> 
<?php include 'footer.php';?>



