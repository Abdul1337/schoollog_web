<?php include 'header.php';?>


<!-- Header Section -->
<div class="inner-page-header">
	<?php include 'header.php';?>
	<div class="clearfix"></div>
	<div class="full">
		<div class="inner-show-img"><img src="assets/images/inner-img08.jpg"></div>
		<div class="container">
			<div class="slider-content">
				<h1>Our Team</h1>
				<p class="subheading">India’s First AI Powered ERP For Schools</p>
			</div>
		</div>
	</div>
	<div class="clearfix"></div>
</div>
<!-- //.Header Section -->
<!--Plan-section-->
<section class="counication-page our-team">
	<div class="container">
		<div class="team-list">
			<div class="team-right-contant-block first-child">
				<div class="date-block date-01">
					<span class="indication"></span>
					<span class="year-month">
						<small>2015</small>
						<big>Jun</big>
					</span>
				</div>
				<p>While helping schools with an NGO called Enactus, Two friends from IIT Ropar
					Mohit Jasapara and Manoj Kumar witnessed the issues faced by schools. And that's
					how the idea of Schoollog was surfaced in room no. 315 of IIT Ropar hostel.</p>
			</div>
		</div>
		<div class="team-list">
			<div class="team-left-contant-block">
				<div class="date-block date-02">
					<span class="indication"></span>
					<span class="year-month">
						<small>2015</small>
						<big>Jul</big>
					</span>
				</div>
				<p>While identifying the user needs, the team came across the barriers affecting
					student learning such as lack of communication between school and parents, no
					availability of management tools for school processes, no easy access to student
					information, etc.
				</p>
			</div>
		</div>
		<div class="team-list">
			<div class="team-right-contant-block">
				<div class="date-block date-03">
					<span class="indication"></span>
					<span class="year-month">
						<small>2015</small>
						<big>Aug</big>
					</span></div>
				<p>Keeping the ultimate goal of enhancing the learning outcome of students in
					mind, the journey of Schoollog began.</p>
			</div>
		</div>
		<div class="team-list">
			<div class="team-left-contant-block">
				<div class="date-block date-04">
					<span class="indication"></span>
					<span class="year-month">
						<small>2015</small>
						<big>Dec</big>
					</span></div>
				<p>Abhishek joined forces with Schoollog to pace up the expansion.
				</p>
			</div>
		</div>
		<div class="team-list">
			<div class="team-right-contant-block">
				<div class="date-block date-05">
					<span class="indication"></span>
					<span class="year-month">
						<small>2016</small>
						<big>Jan</big>
					</span></div>
				<p>Launched the prototype focusing on bridging the gar between Parent-teacher.
					Tied up with earlyvangelist 2 Schools from Ropar. First successful test run of
					Schoollog in schools.</p>
			</div>
		</div>
		<div class="team-list">
			<div class="team-left-contant-block">
				<div class="date-block date-06">
					<span class="indication"></span>
					<span class="year-month">
						<small>2016</small>
						<big>Mar</big>
					</span></div>
				<p>Android and IOS apps were<br />
					launched.
				</p>
			</div>
		</div>
		<div class="team-list">
			<div class="team-right-contant-block">
				<div class="date-block date-07">
					<span class="indication"></span>
					<span class="year-month">
						<small>2016</small>
						<big>Jun</big>
					</span></div>
				<p>After graduating from Engineering and achieving the gratifying results of 70%
					active usage of parents app from earlyvangelists, operations started in Gurugram</p>
			</div>
		</div>
		<div class="team-list">
			<div class="team-left-contant-block">
				<div class="date-block date-08">
					<span class="indication"></span>
					<span class="year-month">
						<small>2016</small>
						<big>Oct</big>
					</span></div>
				<p>With the vision to help schools with the way they manage finances, accounts
					module launched.</p>
			</div>
		</div>
		<div class="team-list">
			<div class="team-right-contant-block">
				<div class="date-block date-09">
					<span class="indication"></span>
					<span class="year-month">
						<small>2017</small>
						<big>Jan</big>
					</span></div>
				<p>With the launch of modules such as Library, Hr, Schoollog takes a step
					forward in automating school processes.</p>
			</div>
		</div>
		<div class="team-list">
			<div class="team-left-contant-block">
				<div class="date-block date-10">
					<span class="indication"></span>
					<span class="year-month">
						<small>2017</small>
						<big>May</big>
					</span></div>
				<p>Biometric Integration launched to enhance the safety of child in school.</p>
			</div>
		</div>
		<div class="team-list">
			<div class="team-right-contant-block">
				<div class="date-block date-06">
					<span class="indication"></span>
					<span class="year-month">
						<small>2017</small>
						<big>Aug</big>
					</span></div>
				<p>AI Powered Enquiry Module Lanched to help schools increase admission in
					upcomeing academic session.</p>
			</div>
		</div>
		<div class="team-list">
			<div class="team-left-contant-block">
				<div class="date-block date-11">
					<span class="indication"></span>
					<span class="year-month">
						<small>2017</small>
						<big>Dec</big>
					</span></div>
				<p>Exploration of the market with operations starting in 3 more cities and with
					the launch of Schoollog 1.0 the mark of schools reached 50.</p>
			</div>
		</div>
		<div class="team-list">
			<div class="team-right-contant-block">
				<div class="date-block date-12">
					<span class="indication"></span>
					<span class="year-month">
						<small>2018</small>
						<big>Mar</big>
					</span></div>
				<p>Launched a website program to facilitate schools with dynamic and responsive
					websites with the website management console.</p>
			</div>
		</div>
		<div class="team-list">
			<div class="team-left-contant-block">
				<div class="date-block date-13">
					<span class="indication"></span>
					<span class="year-month">
						<small>2018</small>
						<big>Aug</big>
					</span></div>
				<p>With market inputs and scalability in sight and valuable feedbacks from
					existing clients, Schoollog Android & IOS apps version 2.0 were launched.</p>
			</div>
		</div>
		<div class="team-list">
			<div class="team-right-contant-block">
				<div class="date-block date-05">
					<span class="indication"></span>
					<span class="year-month">
						<small>2019</small>
						<big>Jan</big>
					</span></div>
				<p>To enhance students’ security at schools, Schoollog launched Visitor
					management sentinel.</p>
			</div>
		</div>
		<div class="team-list">
			<div class="team-left-contant-block">
				<div class="date-block date-14">
					<span class="indication"></span>
					<span class="year-month">
						<small>2019</small>
						<big>Apr</big>
					</span></div>
				<p>With schoollog guaranteed service and dedicated people working to provide the
					best user experience, our number of schools crossed 400.</p>
			</div>
		</div>
		<div class="team-list">
			<div class="team-right-contant-block">
				<div class="date-block date-15">
					<span class="indication"></span>
					<span class="year-month">
						<small>2019</small>
						<big>Jun</big>
					</span></div>
				<p>To improve learning outcomes of every student, Schoollog 3.0 initiated.</p>
			</div>
		</div>
		<div class="team-list">
			<div class="team-left-contant-block">
				<div class="date-block date-16">
					<span class="indication"></span>
					<span class="year-month">
						<small>2019</small>
						<big>Aug</big>
					</span></div>
				<p>Schoollog completes 4 years<br />
					of journey, with team size<br />
					increasing from 2 to 50.</p>
			</div>
		</div>
		<div class="team-list last-child">
			<div class="team-right-contant-block">
				<div class="date-block date-03"></div>
				<p></p>
			</div>
		</div>
	</div>
	<div class="map-tab-bottom why-reasons our-team-detail">
		<div class="container">
			<h2>Meet Our Awesome Team</h2>
			<ul class="team">
			    <div class="slide-carousel">	
					<li>
						<span class="team-pic"><img src="assets/images/team_01.jpg"></span>
						<span class="team-member-info">
							<h3>Abhishek Singh</h3>
							<p class="dagination">Co-founder</p>
						</span>
						<div class="team-member-detail">
							<p>Handles talent acquisition, sales, marketing, strategy. Previous Experience
								of affiliate startup rewardadda</p>
							<a href="https://www.linkedin.com/in/abhischoollog/" target="_blank" class="linkedin-btn">
								<i class="icon-linkedin2"></i>
								Let’s Connect
							</a>
						</div>
					</li>
					<li>
						<span class="team-pic"><img src="assets/images/team_02.jpg"></span>
						<span class="team-member-info">
							<h3>Manoj Kumar</h3>
							<p class="dagination">Co-founder</p>
						</span>
						<div class="team-member-detail">
							<p>Handles Operations, product rnd,finance, investor relations, previous
								experience of working in NGO helping schools called enactus, from IIT ropar</p>
							<a href="https://www.linkedin.com/in/schoollog-manoj/" target="_blank" class="linkedin-btn">
								<i class="icon-linkedin2"></i>
								Let’s Connect
							</a>
						</div>
					</li>
					<li>
						<span class="team-pic"><img src="assets/images/team_03.jpg"></span>
						<span class="team-member-info">
							<h3>Mohit Jasapara</h3>
							<p class="dagination">Co-founder</p>
						</span>
						<div class="team-member-detail">
							<p>Our technocrat handles product development, btech electrical from IIT ropar</p>
							<a href="https://www.linkedin.com/in/mohitjasapara/" target="_blank" class="linkedin-btn">
								<i class="icon-linkedin2"></i>
								Let’s Connect
							</a>
						</div>
					</li>
				</div>
			</ul>
		</div>
	</div>
	<div class="why-reasons career-section">
		<div class="container">
			<h2>Career</h2>
			<div class="career-slide">
				<div class="flexslider carousel">
					<ul class="slides">
						<div class="slide-carousel2">
								<li>
									<a class="opportunity-section">
										<div class="opportunity-box">
											<div class="opportunity-front">
												<div class="opportunity-left">
													<h4>Front end Developer</h4>
													<p>Location : Jaipur</p>
													<p>Job Type : Full Time</p>
													<p>Stream Eligible : B.Tech - Any Stream</p>
												</div>
												<div class="opportunity-img">
													<img src="assets/images/carrer_01.png">
												</div>
											</div>
											<div class="opportunity-back">
												<div class="opportunity-back-title-apply">
													<h4>Responsibilities:</h4>
													<button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal">Apply Now</button>
												</div>
												<p>Building smooth and responsive client applications for web and mobile
													platforms.</p>
												<p>Writing cross browser code that appears consistently in different
													browser.</p>
												<p>Build reusable code and libraries for future use.</p>
											</div>
										</div>
									</a>
								</li>
								<li>
									<a class="opportunity-section" href="javascript:void(0)">
										<div class="opportunity-box">
											<div class="opportunity-front">
												<div class="opportunity-left">
													<h4>Back end Developer</h4>
													<p>Location : Jaipur</p>
													<p>Job Type : Full Time</p>
													<p>Stream Eligible : B.Tech - Any Stream</p>
												</div>
												<div class="opportunity-img">
													<img src="assets/images/carrer_02.png">
												</div>
											</div>
											<div class="opportunity-back">
												<div class="opportunity-back-title-apply">
													<h4>Responsibilities:</h4>
													<button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal2">Apply Now</button>
												</div>
												<p>Delivering programming solutions for a variety of challenging and
													exciting
													projects.</p>
												<p>Testing applications to ensure they meet users' expectations.</p>
												<p>Integrating third party APIs.</p>
											</div>
										</div>
									</a>
								</li>
								<li>
									<a class="opportunity-section" href="javascript:void(0)">
										<div class="opportunity-box">
											<div class="opportunity-front">
												<div class="opportunity-left">
													<h4>Customer Support Exective</h4>
													<p>Location : Jaipur</p>
													<p>Job Type : Full Time</p>
													<p>Stream Eligible : B.TECH - Any Stream</p>
												</div>
												<div class="opportunity-img">
													<img src="assets/images/carrer_01.png">
												</div>
											</div>
											<div class="opportunity-back">
												<div class="opportunity-back-title-apply">
													<h4>Responsibilities:</h4>
													<button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal3">Apply Now</button>
												</div>
												<p>Answer calls professionally to provide information about products and
													services, take/ cancel orders, or obtain details of complaints.</p>
												<p>Keep records of customer interactions and transactions, recording details
													of
													inquiries, complaints, and comments, as well as actions taken. Process
													orders,
													forms and applications.</p>
												<p>Follow up to ensure that appropriate actions were taken on customers'
													requests.</p>
											</div>
										</div>
									</a>
								</li>
						</div>
					</ul>
				</div>
			</div>
		</div>
	</div>
</section>
<!--Plan-section end-->
<!--Model Box Section-->
<div class="modal fade" id="myModal" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Career with Us</h4>
			</div>
			<div class="modal-body">
				<form class="modal-form validate-form contact-form">
					<div class="form-group">
						<input type="hidden" name="type" value="Frontend Developer">
					</div>
					<label>POSITION</label>
					<p class="position-main" id="model-type">FRONT END DEVELOPER</p>
					<?php include "CareerFormFields.php" ?>
					<button class="btn btn-primary" type="submit" name="submit">Send Now</button>
				</form>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="myModal2" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Career with Us</h4>
			</div>
			<div class="modal-body">
				<form class="modal-form validate-form contact-form">
					<div class="form-group">
						<input type="hidden" name="type" value="Backend Developer">
					</div>
					<label>POSITION</label>
					<p class="position-main" id="model-type">BACK END DEVELOPER</p>
					<?php include "CareerFormFields.php" ?>
					<button class="btn btn-primary" type="submit" name="submit">Send Now</button>
				</form>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="myModal3" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Career with Us</h4>
			</div>
			<div class="modal-body">
				<form class="modal-form validate-form contact-form">
					<div class="form-group">
						<input type="hidden" name="type" value="Customer Service Executive">
					</div>
					<label>POSITION</label>
					<p class="position-main" id="model-type">CUSTOMER SERVICE EXECUTIVE</p>
					<?php include "CareerFormFields.php" ?>
					<button class="btn btn-primary" type="submit" name="submit">Send Now</button>
				</form>
			</div>
		</div>
	</div>
</div>
<!-- //.Model Box Section -->
<!-- free-trial-section -->
<section class="free-trial-section wow fadeInUp">
	<?php 
		include 'freeTrialSection.php';
		freeTrialSection("Want to See","How to save time, reduce your workload<br/>and enhance learning?");
		?>
</section>
<!--// free-trial-section -->
<?php include 'footer.php';?>
<script type="text/javascript" src="assets/js/owl.carousel.min.js"></script>

