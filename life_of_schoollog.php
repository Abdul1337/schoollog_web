<?php include 'header.php';?> 
<!-- Header Section -->
    <div class="inner-page-header">
        <div class="full">
            <div class="inner-show-img"><img src="assets/images/lifeAtSchoollog.jpg"></div>
            <div class="container">
                <div class="slider-content">
                    <h1>Life @ Schoollog</h1>
                    <p class="subheading">India’s First AI Powered ERP For Schools.</p>
                </div>
            </div>
			<div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </div> 
    <!-- //.Header Section -->

    <!--Plan-section-->

    <section class="life-schoollog"> 
        <div class="container">
            <div class="our-mission">
                <div class="heading">
                    <h2>Our Mission</h2>
                </div>
                <h3>“To be able to provide level playing field for all educational institutions,
                    rural or urban."</h3>
            </div> 
            <div class="heading">
                <h2>Who we are</h2>
            </div>
            <p>We, the Schoollog squad, is a bunch of highly motivated humans waking up
                every day with a drive of working harder and smarter to enhance the student's
                learning experiences by connecting parents, teachers and the school with our
                cutting edge automation solutions and mobile applications.</p>
            <p>We strive to make the Teachers focus more on academics of a child by getting
                rid of smaller yet time consuming daily chores like marking attendance, filling
                up students diary etc. and keeping Parents aware and involved in child's
                progress which eventually satisfies the purpose of enhancing a child's learning.</p>
        </div>

        <div class="counication-page">
            <div class="container">
                <h2>Culture</h2>

                <div class="comunication-step">
                    <div class="step-image">
                        <img src="assets/images/work-icon.png">
                    </div>
                    <div class="step-content">
                        <h3>Work spirit</h3>
                        <p>We love what we do and are so proud to revolutionize the education sector
                            with our dream to provide our children with a better infrastructure of learning
                            with trailblazing technologies so that they can shape our society better.
                        </p>
                    </div>
                </div>

                <div class="comunication-step">
                    <div class="step-image right-side">
                        <img src="assets/images/fun.png">
                    </div>

                    <div class="step-content left-side">
                        <h3>Unlimited fun</h3>
                        <p>We believe in Work-life balance. That's why we have fun as much as we work.
                            Regular outings, movie sessions, gaming sessions never let our energy go down!
                            Be it work or be it fun, Schoollog team is always up for it.</p>
                    </div>
                </div>

                <div class="comunication-step">
                    <div class="step-image">
                        <img src="assets/images/apperciation.png">
                    </div>

                    <div class="step-content">
                        <h3>Think out of the box culture​</h3>
                        <p>Here at Schoollog everybody is encouraged to bring the innovation to the
                            table.Experiments, learning and work go side by side.</p>
                    </div>
                </div>

                <div class="comunication-step">

                    <div class="step-image right-side">
                        <img src="assets/images/money.png">
                    </div>

                    <div class="step-content left-side">
                        <h3>No bars to growth​</h3>
                        <p>We foster a learning culture which brings a major shift in the skill set of
                            every individual becoming a part of Schoollog family. The more you learn, grow
                            and work, the more you get paid off.</p>
                    </div> 
                </div>

                <div class="comunication-step">
                    <div class="step-image">
                        <img src="assets/images/hours.png">
                    </div>
                    <div class="step-content">
                        <h3>Work appreciation</h3>
                        <p>We celebrate even your smallest accomplishments because we know it's the
                            little things that make the difference. Schoollog fam loves the happy bunch who
                            is ready to do the wonders.</p>
                    </div>
                </div>

            </div>
        </div>

    </section>
    <!--Plan-section end--> 

<!-- free-trial-section -->
<section class="free-trial-section wow fadeInUp">
	 <?php 
		include 'freeTrialSection.php';
		freeTrialSection("Want to See","Excited to be a part of the young and motivated Schoollog team?");
		?>
</section> 
<!--// free-trial-section --> 
<?php include 'footer.php';?>

