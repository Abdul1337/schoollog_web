<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>SchoolLog | School ERP & LMS</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <link rel="shortcut icon" href="assets/images/favicon.ico">
    <!-- Bace Css for this template -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/css/custom.css" rel="stylesheet">
    <link href="assets/css/custom-responsive.css" rel="stylesheet">
</head>
<body>
    <!-- Header Section -->
    <section id="section01">
        <?php include 'header.php';?>
        <div class="Downloads-container">
            <div class="app-card-container">
                <div class="flex-row row-0">
                    <div tabindex="0" id="app-0" class="app-card app-card-active">
                        <p class="app-name">Parent App</p>
                    </div>
                    <div tabindex="0" id="app-1" class="app-card">
                        <p class="app-name">Teacher App</p>
                    </div>
                </div>
                <div class="flex-row row-1">
                    <div tabindex="0" id="app-2" class="app-card">
                        <p class="app-name">Director App</p>
                    </div>
                    <div tabindex="0" id="app-3" class="app-card">
                        <p class="app-name">Admin App</p>
                    </div>
                </div>
            </div>
            <div class="app-info">
                <div class="app-logo-name">
                    <div class="app-logo">
                        <img src="assets/images/awesome-app/parent-app-icon.png" alt="">
                    </div>
                    <div class="info-app-name">
                        <p>Parent App</p>
                    </div>
                </div>
                <div class="app-info-links">
                    <div class="info-app-description"></div>
                    <div class="download-links">
                        <div class="appstore store-link">
                            <a class="store-badge" id="iOS"
                                href="https://apps.apple.com/in/app/schoollog-parent-app/id1168033671" target="_blank">
                                <img src="assets/images/awesome-app/appstore-badge.png" alt="Download on the App Store">
                            </a>
                        </div>
                        <div class="playstore store-link">
                            <a class=" store-badge" id="Android"
                            href="https://play.google.com/store/apps/details?id=in.schoollog.android.parentapp"
                            target="_blank">
                            <img src="assets/images/awesome-app/playstore-badge.png" alt="Get it on Play Store">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            </ class="Downloads-container">
    </section>
    <!-- //.Header Section -->
    <!-- Footer Section-->
    <section class="free-trial-section">
        <?php 
		include 'freeTrialSection.php';
		freeTrialSection("Want to See","Excited to be a part of the young and motivated Schoollog team?");
		?>
    </section>
    <div class="clearfix"></div>
    <?php include 'footer.php';?>
    <a href="#" id="scroll" style="display: none;">
        <span></span></a>
    <!--Main Section End-->
    <script type="text/javascript" src="assets/js/jquery.min.js.download"></script>
    <script type="text/javascript" src="assets/js/jquery/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="assets/js/custom.js"></script>
</body>
</html>