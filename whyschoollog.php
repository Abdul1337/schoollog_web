<?php include 'header.php';?>  
   <!-- Banner Section --> 
    <div class="inner-page-header">
        <div class="full">
            <div class="inner-show-img"><img src="assets/images/whySchoollog.jpg"></div>
            <div class="container">
                <div class="slider-content">
                    <h1>Why Schoollog?</h1>
                    <p class="subheading">Amazed to see why Schoollog is loved by 1000+ schools
                        across India? Let's find out!</p>
                </div>
            </div>
			<div class="clearfix"></div>
        </div>
        
		<div class="clearfix"></div>
    </div> 
    <!-- //.Banner Section -->

    <!--Plan-section-->

    <section class="counication-page">
        <section class="blank-space"></section>
        <div class="container">
            <div class="comunication-step">
                <div class="step-image left-side">
                    <img src="assets/images/whyschoollog_01.png">

                </div>
                <div class="step-content right-side">

                    <h3>Boost Admissions with your branded app</h3>
                    <p>Spending a lot on marketing? Why not make a smart move by branding your
                        school with its application? Get your school's personalized smart, interactive
                        and easy to use parents application listed on Playstore and App store with your
                        school’s logo and see the happy parents do the rest of the marketing for you.</p>
                </div>
            </div>

            <div class="comunication-step">
                <div class="step-image right-side">
                    <img src="assets/images/whyschoollog_02.png">
                </div>
                <div class="step-content left-side">

                    <h3>Easy set-up and use</h3>
                    <p>With no hardware installation required, you can use Schoollog from any corner
                        of the earth with just an internet-connected device. Its intuitive user
                        interface allows you to learn the operations on your own. On top of it, our
                        self-help product videos familiarise you with all the features in detail. Anyone
                        with fundamental knowledge of computers will be easily able to use Schoollog.</p>
                </div>
            </div>

            <div class="comunication-step">
                <div class="step-image left-side">
                    <img src="assets/images/whyschoollog_03.png">
                </div>
                <div class="step-content right-side">

                    <h3>HTTPS/SSH Secure Platform</h3>
                    <p>All the school information and data are kept encrypted on highly secured
                        Amazon web servers preventing it from any malicious use. Cloud storage allows
                        you to access the data from anywhere at any time with any internet-connected
                        device.</p>
                </div>
            </div>

            <div class="comunication-step">
                <div class="step-image right-side">
                    <img src="assets/images/whyschoollog_04.png">
                </div>
                <div class="step-content left-side">

                    <h3>Extensive customer-friendly support</h3>
                    <p>On taking a school onboard Schoollog not only helps you with data update and
                        initial trainings but to bring you the best support experience and get your
                        issues resolved instantly, Schoollog has a dedicated team who are there for you
                        24*7. On top of that, no bots, you'll be talking to real people assisting you
                        throughout the process.</p>
                </div>
            </div>

            <div class="comunication-step">
                <div class="step-image left-side">
                    <img src="assets/images/whyschoollog_05.png">
                </div>
                <div class="step-content right-side">

                    <h3>​Web portal and mobile applications</h3>
                    <p>​ With Schoollog you get personalised school mobile apps and web portals with
                        login for Parent, Teacher, Management and all other stakeholders of a school
                        giving realtime information and alerts for latest happening at the school.</p>
                </div>
            </div>

            <div class="comunication-step last-child"></div>

        </div>
        <div class="map-tab-bottom why-reasons more-reason-required">
            <div class="container">
                <h2>More Reasons Required?</h2>
                <ul class="reasons-reuired">
                    <div class="slide-carousel">
                        <li>
                             <i class="icon-mobile"></i>
                            <span class="list-title">Apps and modules for all</span>
                        </li>

                        <li>
                            <i class="icon-more_reasons_02"></i>
                            <span class="list-title">Customization</span>
                        </li>

                        <li>
                            <i class="icon-more_reasons_03"></i>
                            <span class="list-title">Completely digital</span>
                        </li>

                        <li>
                            <i class="icon-more_reasons_04"></i>
                            <span class="list-title">Technology aided schooling</span>
                        </li>
                        <li>

                            <i class="icon-more_reasons_05"></i>

                            <span class="list-title">1000+ printable reports</span>
                        </li>

                        <li>
                            <i class="icon-more_reasons_06"></i>
                            <span class="list-title">Security and data back-up</span>
                        </li>

                        <li>

                            <i class="icon-more_reasons_07"></i>
                            <span class="list-title">Biometric integration</span>
                        </li>

                        <li>
                            <i class="icon-more_reasons_08"></i>
                            <span class="list-title">Completely paperless</span>
                        </li>
                    </div>
                </ul>
            </div>

            <!-- <div class="panel-group" id="accordion" role="tablist"
            aria-multiselectable="true"></div> -->
        </div>

    </section>
    <!--Plan-section end-->


<!-- free-trial-section -->
<section class="free-trial-section wow fadeInUp">
	 <?php 
		include 'freeTrialSection.php';
		freeTrialSection("More Reasons?","Still need more reasons to love us? Click on the button and let us show you how we roll!");
		?>
</section> 
<!--// free-trial-section --> 
<?php include 'footer.php';?>
<script type="text/javascript" src="assets/js/owl.carousel.min.js"></script>



